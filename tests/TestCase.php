<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    public function testRequiredFieldsForRegistration()
    {
        $this->json('POST', 'api/customers', ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "fullname" => ["The fullname field is required."],
                    "email" => ["The email field is required."],
                    "username" => ["The username field is required."],
                    "gender" => ["The gender field is required."],
                    "country" => ["The country field is required."],
                    "city" => ["The city field is required."],
                    "phone" => ["The phone field is required."],
                ]
            ]);
    }
}
