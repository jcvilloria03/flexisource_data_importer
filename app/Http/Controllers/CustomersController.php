<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class CustomersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {
        $request = request();

        $page = $request->input('page');
        $sortBy = $request->input('sort_by');
        $perPage = $request->input('per_page');
        $query = Customer::select('fullname','email','country')->where('fullname', '!=', null);
        if (!empty($sortBy)) {
            $sort = explode('.', $sortBy);
            $query->orderBy($sort[0], $sort[1]);
        }

        if (!empty($request->input('group_by'))) {
            $query->groupBy($request->input('group_by'));
        }

        $query = $query->paginate(!empty($perPage) ? $perPage : $query->count(), ['*'], 'page', !empty($page) ? $page : 1);
        return $query;
    }

    public function get($id)
    {
        $customer = Customer::select('fullname','email','username','gender','country','city','phone')->find($id);
        if (empty($customer)) {
            abort(404, "Customer not found");
        }

        return $customer;
    }

    public function update(Request $request, $email)
    {
        $inputs = $request->all();
        if (!empty($inputs)) {
            try {
                $customer = Customer::where('email', '=', $email);
                $customer->update($inputs);
            } catch (Throwable $e) {
                abort(500, $e->getMessage());
            }

        }

        return response(200);
    }

    public function populate(Request $request)
    {
        $nationality = "au";
        $link = "https://randomuser.me/api/?results=".$request->rCount."&nat=".$nationality;
        $json = json_decode(file_get_contents($link), true);

        for($i=0;$i<count($json['results']);$i++){
            $fullname = $json['results'][$i]['name']['first'] . ' ' . $json['results'][$i]['name']['last'];
            $email = $json['results'][$i]['email'];
            $username = $json['results'][$i]['login']['username'];
            $gender = $json['results'][$i]['gender'];
            $country = $json['results'][$i]['location']['country'];
            $city = $json['results'][$i]['location']['city'];
            $phone = $json['results'][$i]['phone'];

            $customer = Customer::where('email', '=', $email)->first();
            if (empty($customer)) {
                try {
                    $newcustomer = new Customer();
                    $newcustomer->fullname = $fullname;
                    $newcustomer->email = $email;
                    $newcustomer->username = $username;
                    $newcustomer->gender = $gender;
                    $newcustomer->country = $country;
                    $newcustomer->city = $city;
                    $newcustomer->phone = $phone;
                    $newcustomer->save();
                } catch (Throwable $e) {
                    abort(500, $e->getMessage());
                }
            } else {
                try {
                    $customer->fullname = $fullname;
                    $customer->email = $email;
                    $customer->username = $username;
                    $customer->gender = $gender;
                    $customer->country = $country;
                    $customer->city = $city;
                    $customer->phone = $phone;
                    $customer->save();
                } catch (Throwable $e) {
                    abort(500, $e->getMessage());
                }
            }
        }

        return response(200);
    }
}
