<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $fillable = [
        'fullname',
        'email',
        'username',
        'gender',
        'country',
        'city',
        'phone',
    ];
}
