CREATE DATABASE  IF NOT EXISTS `onlinebetting` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `onlinebetting`;
-- MySQL dump 10.13  Distrib 8.0.21, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: onlinebetting
-- ------------------------------------------------------
-- Server version	5.7.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `amount_types`
--

DROP TABLE IF EXISTS `amount_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `amount_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `amount` decimal(15,2) DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amount_types`
--

LOCK TABLES `amount_types` WRITE;
/*!40000 ALTER TABLE `amount_types` DISABLE KEYS */;
INSERT INTO `amount_types` VALUES (1,2.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(2,25.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(3,50.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(4,250.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(5,500.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(6,1000.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(7,5000.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(8,10000.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(9,50000.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(10,80000.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(11,100000.00,'2020-09-16 11:36:10','2020-09-16 11:36:10');
/*!40000 ALTER TABLE `amount_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bet_game_horse`
--

DROP TABLE IF EXISTS `bet_game_horse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bet_game_horse` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `bet_id` bigint(20) unsigned NOT NULL,
  `game_id` bigint(20) unsigned NOT NULL,
  `horse_id` bigint(20) unsigned NOT NULL,
  `position_number` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bet_game_horse_bet_id_foreign` (`bet_id`),
  KEY `bet_game_horse_game_id_foreign` (`game_id`),
  KEY `bet_game_horse_horse_id_foreign` (`horse_id`),
  CONSTRAINT `bet_game_horse_bet_id_foreign` FOREIGN KEY (`bet_id`) REFERENCES `bets` (`id`),
  CONSTRAINT `bet_game_horse_game_id_foreign` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`),
  CONSTRAINT `bet_game_horse_horse_id_foreign` FOREIGN KEY (`horse_id`) REFERENCES `horses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=211 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bet_game_horse`
--

LOCK TABLES `bet_game_horse` WRITE;
/*!40000 ALTER TABLE `bet_game_horse` DISABLE KEYS */;
INSERT INTO `bet_game_horse` VALUES (189,77,1,4,5),(190,77,1,2,6),(191,77,2,3,3),(192,77,2,4,4),(193,78,1,4,5),(194,78,1,2,6),(195,78,2,3,3),(196,78,2,4,4),(197,79,1,4,5),(198,79,1,2,6),(199,79,2,3,3),(200,79,2,4,4),(201,80,1,4,5),(202,80,1,2,6),(203,80,2,3,3),(204,80,2,4,4),(205,81,1,4,5),(206,81,1,2,6),(207,81,2,3,3),(208,81,2,4,4),(209,82,1,1,1),(210,83,1,1,1);
/*!40000 ALTER TABLE `bet_game_horse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bet_types`
--

DROP TABLE IF EXISTS `bet_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bet_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `formula` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `game_count` int(11) NOT NULL DEFAULT '1',
  `horse_count` int(11) NOT NULL DEFAULT '1',
  `select_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `select_input` json NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bet_types`
--

LOCK TABLES `bet_types` WRITE;
/*!40000 ALTER TABLE `bet_types` DISABLE KEYS */;
INSERT INTO `bet_types` VALUES (1,'WIN',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10',1,1,'each','[1]','#e1b304',1),(2,'FORECAST',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10',1,2,'each','[1, 2]','#9bbdbc',0),(3,'QUARTET',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10',1,4,'each','[1, 2, 3, 4]','#cf7c27',0),(4,'PENTAFECTA',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10',1,5,'each','[1, 2, 3, 4, 5]','#536c81',0),(5,'SUPER 6',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10',1,6,'each','[1, 2, 3, 4, 5, 6]','#6abcd9',0),(6,'DAILY DOUBLE',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10',2,1,'each','[1]','#11a8fc',0),(7,'WINNER TAKE ALL',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10',7,1,'each','[1]','#8fec11',1),(11,'PICK SIX',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10',6,1,'each','[1]','#513133',0),(12,'PICK FIVE',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10',5,1,'each','[1]','#36038f',0);
/*!40000 ALTER TABLE `bet_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bets`
--

DROP TABLE IF EXISTS `bets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PHP',
  `win_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'Pending',
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `winning_amount` decimal(15,2) DEFAULT '0.00',
  `event_id` bigint(20) unsigned NOT NULL,
  `bet_type_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bets_id_unique` (`id`),
  KEY `bets_user_id_foreign` (`user_id`),
  KEY `bets_event_id_foreign` (`event_id`),
  KEY `bets_bet_type_id_foreign` (`bet_type_id`),
  CONSTRAINT `bets_bet_type_id_foreign` FOREIGN KEY (`bet_type_id`) REFERENCES `bet_types` (`id`),
  CONSTRAINT `bets_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`),
  CONSTRAINT `bets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bets`
--

LOCK TABLES `bets` WRITE;
/*!40000 ALTER TABLE `bets` DISABLE KEYS */;
INSERT INTO `bets` VALUES (77,1,0.20,'PHP','Pending',NULL,'2020-09-16 15:26:54','2020-09-16 15:26:54',0.00,1,6),(78,1,0.20,'PHP','Pending',NULL,'2020-09-16 15:34:28','2020-09-16 15:34:28',0.00,1,6),(79,1,0.20,'PHP','Pending',NULL,'2020-09-16 15:57:37','2020-09-16 15:57:37',0.00,1,6),(80,1,0.20,'PHP','Pending',NULL,'2020-09-16 15:57:56','2020-09-16 15:57:56',0.00,1,6),(81,1,0.20,'PHP','Pending',NULL,'2020-09-16 16:04:31','2020-09-16 16:04:31',0.00,1,6),(82,1,100.00,'PHP','Pending',NULL,'2020-09-16 16:53:58','2020-09-16 16:53:58',0.00,1,1),(83,1,100.00,'PHP','Pending',NULL,'2020-09-16 16:54:33','2020-09-16 16:54:33',0.00,1,1);
/*!40000 ALTER TABLE `bets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `claim_requests`
--

DROP TABLE IF EXISTS `claim_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `claim_requests` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `account_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_processed` tinyint(1) NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'requested',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `claim_requests`
--

LOCK TABLES `claim_requests` WRITE;
/*!40000 ALTER TABLE `claim_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `claim_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `events` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'STA. MESA','Description 1','2020-09-16 11:36:10','2020-09-16 11:36:10'),(2,'Carmona','Description 2','2020-09-16 11:36:10','2020-09-16 11:36:10'),(3,'STA. ANA','Description 3','2020-09-16 11:36:10','2020-09-16 11:36:10');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_horse`
--

DROP TABLE IF EXISTS `game_horse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `game_horse` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `game_id` bigint(20) unsigned NOT NULL,
  `horse_id` bigint(20) unsigned NOT NULL,
  `horse_no` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`),
  KEY `game_horse_game_id_foreign` (`game_id`),
  KEY `game_horse_horse_id_foreign` (`horse_id`),
  CONSTRAINT `game_horse_game_id_foreign` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`),
  CONSTRAINT `game_horse_horse_id_foreign` FOREIGN KEY (`horse_id`) REFERENCES `horses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_horse`
--

LOCK TABLES `game_horse` WRITE;
/*!40000 ALTER TABLE `game_horse` DISABLE KEYS */;
INSERT INTO `game_horse` VALUES (1,1,1,1,'scratched'),(2,1,2,2,'active'),(3,1,3,3,'active'),(4,1,4,4,'active'),(5,1,5,5,'active'),(6,1,6,6,'active'),(7,2,1,1,'scratched'),(8,2,4,2,'active'),(9,2,3,3,'active'),(10,2,8,4,'active'),(11,2,7,5,'active'),(27,3,1,1,'active'),(28,3,4,2,'active'),(29,3,3,3,'active'),(30,3,8,4,'active'),(31,3,7,5,'active'),(32,4,1,1,'active'),(33,4,4,2,'active'),(34,4,3,3,'active'),(35,4,8,4,'active'),(36,4,7,5,'active'),(37,5,1,1,'active'),(38,5,4,2,'active'),(39,5,3,3,'active'),(40,5,8,4,'active'),(41,5,7,5,'active'),(62,11,1,1,'active'),(63,11,4,2,'active'),(64,11,3,3,'active'),(65,11,8,4,'active'),(66,11,7,5,'active'),(67,12,1,1,'active'),(68,12,4,2,'active'),(69,12,3,3,'active'),(70,12,8,4,'active'),(71,12,7,5,'active');
/*!40000 ALTER TABLE `game_horse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `games`
--

DROP TABLE IF EXISTS `games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `games` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL,
  `timezone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `generate_winners_lock` tinyint(1) NOT NULL DEFAULT '0',
  `other_information` json DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `livestream_url` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bet_types` json DEFAULT NULL,
  `odd_types` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `event_id` bigint(20) unsigned DEFAULT NULL,
  `game_number` int(11) NOT NULL,
  `replay_url` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `games_event_id_foreign` (`event_id`),
  CONSTRAINT `games_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `games`
--

LOCK TABLES `games` WRITE;
/*!40000 ALTER TABLE `games` DISABLE KEYS */;
INSERT INTO `games` VALUES (1,'R1',NULL,'2020-09-16 00:00:00',NULL,'PHT',0,NULL,NULL,NULL,'pending',NULL,NULL,'2020-09-16 12:11:27','2020-09-16 12:11:27',1,1,'https://www.youtube.com/watch?v=VMAk1_bcv90&ab_channel=NBCSports'),(2,'R2',NULL,'2020-09-14 00:00:00',NULL,'PHT',0,NULL,NULL,NULL,'pending',NULL,NULL,'2020-09-16 12:12:05','2020-09-16 12:12:05',1,2,NULL),(3,'R3',NULL,'2020-09-14 00:00:00',NULL,'PHT',0,NULL,NULL,NULL,'pending',NULL,NULL,'2020-09-16 12:12:05','2020-09-16 12:12:05',1,3,NULL),(4,'R4',NULL,'2020-09-14 00:00:00',NULL,'PHT',0,NULL,NULL,NULL,'pending',NULL,NULL,'2020-09-16 12:12:05','2020-09-16 12:12:05',1,4,NULL),(5,'R5',NULL,'2020-09-14 00:00:00',NULL,'PHT',0,NULL,NULL,NULL,'pending',NULL,NULL,'2020-09-16 12:12:05','2020-09-16 12:12:05',1,5,NULL),(11,'R6',NULL,'2020-09-14 00:00:00',NULL,'PHT',0,NULL,NULL,NULL,'pending',NULL,NULL,'2020-09-16 12:12:05','2020-09-16 12:12:05',1,6,NULL),(12,'R7',NULL,'2020-09-14 00:00:00',NULL,'PHT',0,NULL,NULL,NULL,'pending',NULL,NULL,'2020-09-16 12:12:05','2020-09-16 12:12:05',1,7,NULL);
/*!40000 ALTER TABLE `games` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horse_images`
--

DROP TABLE IF EXISTS `horse_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `horse_images` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `horse_id` bigint(20) unsigned NOT NULL,
  `image_path` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `horse_images_horse_id_foreign` (`horse_id`),
  CONSTRAINT `horse_images_horse_id_foreign` FOREIGN KEY (`horse_id`) REFERENCES `horses` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horse_images`
--

LOCK TABLES `horse_images` WRITE;
/*!40000 ALTER TABLE `horse_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `horse_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horses`
--

DROP TABLE IF EXISTS `horses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `horses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `statistics` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sire` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dam` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trainer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_prize_money` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wins` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `intro` text COLLATE utf8mb4_unicode_ci,
  `career` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horses`
--

LOCK TABLES `horses` WRITE;
/*!40000 ALTER TABLE `horses` DISABLE KEYS */;
INSERT INTO `horses` VALUES (1,'A SHIN MARCHING','3yo DK B Filly',NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10','Eishin Flash (JPN) 2007','Massillon Girl (USA) 2003 [By With Approval (CAN) 1986]','Yasuyuki Takahashi','Eishindo Co. Ltd.','Japan : JPY ¥366,000','1400m (1)','A Shin Marching is a thoroughbred horse born in Japan in 2017. Race horse A Shin Marching is by Eishin Flash (JPN) out of Massillon Girl (USA) , trained by Yasuyuki Takahashi. A Shin Marching form is available here. Owned by Eishindo Co. Ltd..','1 Win (11%) - No Places (11%) - 9 starts',NULL),(2,'ABRACADAZZLE','3yo B Filly',NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10','Sizzling (AUS) 2009','	Abracadash (AUS) 2011 [By Magic Albert (AUS) 1998]','Tony Gollan','Mrs D M Power & Mrs L K Barton','AUD $21,200','1400m (1)','Abracadazzle is a thoroughbred horse born in Australia in 2017. Race horse Abracadazzle is by Sizzling (AUS) out of Abracadash (AUS) , trained by Tony Gollan. Abracadazzle form is available here. Owned by Mrs D M Power & Mrs L K Barton.','1 Win (33%) - No Places (33%) - 3 starts',NULL),(3,'LUCKY IMAGINATION','3yo GR/RO Filly',NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10','Imagining (USA) 2008','Lucky Chick (USA)2000 [By Two Punch (USA) 1983]','Patricia Farro','Vaccaro Racing Stable & Patricia Farro','AUD $43,970','1408m (1)','Lucky Imagination is a thoroughbred horse born in United States of America in 2017. Race horse Lucky Imagination is by Imagining (USA) out of Lucky Chick (USA) , trained by Patricia Farro. Lucky Imagination form is available here. Owned by VACCARO RACING STABLE & PATRICIA FARRO.','1 Win (33%) - No Places (33%) - 3 starts',NULL),(4,'MAMA\'S COFFEE','2yo B Filly',NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10','Casino Drive (USA) 2005','Mama\'s Dish (JPN) 2007','Noboru Yonekawa','Noboru Yonekawa','AUD $12,846','1200m (1)','Mama\'s Coffee is a thoroughbred horse born in Japan in 2018. Race horse Mama\'s Coffee is by Casino Drive (USA) out of Mama\'s Dish (JPN) , trained by Noboru Yonekawa. Mama\'s Coffee form is available here. Owned by .','1 Win (33%) - 2 Placings (100%) - 3 starts',NULL),(5,'BEAR THE CROWN','4yo CH Mare',NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10','Olympic Glory (IRE) 2010','Mosscedes (AUS) 2006 [By Mossman (AUS) 1995]','Barry Ratcliff','B J Ratcliff & S M Foster','AUD $29,400','1000m (1), 1500m (1)','Bear The Crown is a thoroughbred horse born in Australia in 2016. Race horse Bear The Crown is by Olympic Glory (IRE) out of Mosscedes (AUS) , trained by Barry Ratcliff. Bear The Crown form is available here. Owned by B J Ratcliff & S M Foster.','2 Wins (29%) - 1 Place (43%) - 7 starts',NULL),(6,'CAMPARI SODA','5yo B Mare',NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10','Dream Ahead (USA) 2008','Chitraka (AUS) 2007 [By Tale of The Cat (USA) 1994]','Barry Ratcliff','B J Ratcliff & A J Ahearn','AUD $55,920','1000m (1), 1106m (1), 1250m (1)','Campari Soda is a thoroughbred horse born in Australia in 2015. Race horse Campari Soda is by Dream Ahead (USA) out of Chitraka (AUS) , trained by Barry Ratcliff. Campari Soda form is available here. Owned by B J Ratcliff & A J Ahearn.','3 Wins (17%) - 6 Placings (50%) - 18 starts',NULL),(7,'PRETTY RUNAWAY','3yo SOR Filly',NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10','Winners Award (USA) 2002','Pretty Jane Perry [By Pretty Boy Perry (USA) 1999]','Jason Pascoe','Heste Sport Inc','AUD $59,348','274m (1), 302m (1), 366m (1)','Pretty Runaway is a thoroughbred horse born in Canada in 2017. Race horse Pretty Runaway is by Winners Award (USA) out of Pretty Jane Perry , trained by Jason Pascoe. Pretty Runaway form is available here. Owned by HESTE SPORT INC.','3 Wins (33%) - 3 Placings (67%) - 9 starts',NULL),(8,'RESILIENCE BLUE','3yo DK B Colt',NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10','Unrivaled (JPN) 2006','B B Cygnus (JPN) 2002 [By White Muzzle (GB) 1990]','Masaki Matsuyama','Ygg Horse Club Co. Ltd.','AUD $39,496','1500m (1)','Resilience Blue is a thoroughbred horse born in Japan in 2017. Race horse Resilience Blue is by Unrivaled (JPN) out of B B Cygnus (JPN) , trained by Masaki Matsuyama. Resilience Blue form is available here. Owned by YGG Horse Club Co. Ltd..','1 Win (25%) - 1 Place (50%) - 4 starts',NULL);
/*!40000 ALTER TABLE `horses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_06_01_000001_create_oauth_auth_codes_table',1),(4,'2016_06_01_000002_create_oauth_access_tokens_table',1),(5,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(6,'2016_06_01_000004_create_oauth_clients_table',1),(7,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(8,'2019_08_19_000000_create_failed_jobs_table',1),(9,'2020_09_03_121503_create_status_types_table',1),(10,'2020_09_03_132008_create_games_table',1),(11,'2020_09_03_132451_create_horses_table',1),(12,'2020_09_03_133522_create_bets_table',1),(13,'2020_09_03_133552_create_winners_table',1),(14,'2020_09_03_134945_create_horse_images_table',1),(15,'2020_09_05_063817_create_games_horses_table',1),(16,'2020_09_05_124142_alter_bet_table_add_winning_amount_column',1),(17,'2020_09_08_044647_create_events_table',1),(18,'2020_09_08_062249_alter_games_table_add_event_id_column',1),(19,'2020_09_08_074510_create_bet_types_table',1),(20,'2020_09_08_075054_create_amount_types_table',1),(21,'2020_09_09_144901_alter_horses_table_add_columns',1),(22,'2020_09_11_121454_create_claim_requests_table',1),(23,'2020_09_12_071358_alter_horses_table_add_logo_column',1),(24,'2020_09_13_032217_alter_game_horse_table_add_horse_no_column',1),(25,'2020_09_16_100211_alter_bet_type_table_add_game_count_and_horse_count',1),(26,'2020_09_16_104846_create_bet_game_horse_pivot_table',1),(27,'2020_09_16_112430_alter_bet_table_add_event_id_column',1),(28,'2020_09_19_060323_alter_games_table_add_game_number_column',2),(29,'2020_09_19_093141_alter_bet_types_table_add_select_type_select_input',2),(30,'2020_09_19_100352_alter_bet_types_table_add_color_column',3),(31,'2020_09_19_150648_alter_bet_types_add_enabled_column',4),(32,'2020_09_20_034553_alter_games_table_add_replay_url',5),(34,'2020_09_20_094504_alter_users_table_add_user_type_column',6),(35,'2020_09_20_112159_alter_game_horse_table_add_status_column',7);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('0018ddac308d54b4f33ed0cf5e820efa3eaa4e028f6a9388477912181fb1e633c41f943871b28de9',9,1,'authToken','[]',0,'2020-09-20 10:58:56','2020-09-20 10:58:56','2021-09-20 10:58:56'),('00384f74038908d071b8a66a6ff670e8ac569c202c515926c94066ded3d30d67ee514f03d2472abf',1,1,'authToken','[]',1,'2020-09-16 17:24:03','2020-09-16 17:24:03','2021-09-16 17:24:03'),('00819709a9ce99f2fec33cf30c3c8067a6380ed50d1af714fbbdbb8c675887d2307735c418fd1e2f',1,1,'authToken','[]',0,'2020-09-18 14:37:21','2020-09-18 14:37:21','2021-09-18 14:37:21'),('02177c5477893d706cd37e8214f126c239832b400b2a82cf7fab84b5fea4be6b5a95fd0db54e466d',1,1,'authToken','[]',1,'2020-09-16 15:58:09','2020-09-16 15:58:09','2021-09-16 15:58:09'),('02238ca822944449f237b9d589eb5822c5a58ad9d5cf0217a18c91377969e6ec21f097df6f0ef015',1,1,'authToken','[]',1,'2020-09-16 15:51:16','2020-09-16 15:51:16','2021-09-16 15:51:16'),('02e23213febfdc9fd26f0b0d406bf4a0f69944bf488cbf0b7d969a95899d7bf2750fd78ace5fdfc8',1,1,'authToken','[]',0,'2020-09-20 07:07:12','2020-09-20 07:07:12','2021-09-20 07:07:12'),('03bd31503136ee7bfdd7b43e4bf620bb19c033dac5a9d16f7ac38f65460e63d96bf932220e08a4d3',1,1,'authToken','[]',0,'2020-09-20 07:58:07','2020-09-20 07:58:07','2021-09-20 07:58:07'),('03c76c67e7c817c3b6873ac8bfca2640ef5fd0fe2eabfcbcb97cf76d2360768f1d3344e2d52c73cf',1,1,'authToken','[]',0,'2020-09-20 07:41:30','2020-09-20 07:41:30','2021-09-20 07:41:30'),('04847b2b6959bbf51f9510a1af4ae566b4709b73ae0c409c7b83accd27cf70aecfd1e8ad04e014d0',1,1,'authToken','[]',1,'2020-09-16 13:34:54','2020-09-16 13:34:54','2021-09-16 13:34:54'),('048555d59057af8c24fcd629bb6bcdaf8d355f68273bbd578f648d7b1588cf63b7264f9b03e769a8',1,1,'authToken','[]',1,'2020-09-16 17:22:27','2020-09-16 17:22:27','2021-09-16 17:22:27'),('04f9253376c37c2928b559b678ed6ac854efc7a766d146ea06bae4815ddae060f0a394bb876b4bc7',1,1,'authToken','[]',0,'2020-09-17 12:20:59','2020-09-17 12:20:59','2021-09-17 12:20:59'),('064ebbe08b9e9e7454a8ac8ffde558a7a63eae4283b7a31c86f21853597a47d171c25e7044fb8605',1,1,'authToken','[]',1,'2020-09-16 16:49:08','2020-09-16 16:49:08','2021-09-16 16:49:08'),('0735ea76027f3653caddcdecc1e3c6528201dfa3343b1080d32c166b63816783e5c84e5a78f64c2c',1,1,'authToken','[]',0,'2020-09-20 08:13:21','2020-09-20 08:13:21','2021-09-20 08:13:21'),('087b6fe7a60a6ca579e06692619bf8c19d7760abad8c1c8db5a5c6c63c94b971b44d91c908bac485',1,1,'authToken','[]',1,'2020-09-16 13:40:28','2020-09-16 13:40:28','2021-09-16 13:40:28'),('09fb7d6915cba3b10df42c3c92233d85d1e98ea1c2f945bdef6275ad3ec152160e8216da9df48359',1,1,'authToken','[]',0,'2020-09-17 12:00:49','2020-09-17 12:00:49','2021-09-17 12:00:49'),('0bb657a2175bd7516e2c02ee2ac0c6887f7f2d1cce99da3134d61a74ca3aca4659cf562fa736f123',1,1,'authToken','[]',1,'2020-09-16 17:28:58','2020-09-16 17:28:58','2021-09-16 17:28:58'),('0be03d9f0e5f62b270fdcb44143878aa911e47d7e283d84aeba3802c37783a822df1cab84a83be90',1,1,'authToken','[]',1,'2020-09-16 17:24:48','2020-09-16 17:24:48','2021-09-16 17:24:48'),('0c072e733ca7103ddbe32544715b2ebdcdfdb2f06547ebdf77c8d3bde17bc75ebe2a18c2592767f8',1,1,'authToken','[]',0,'2020-09-17 11:18:30','2020-09-17 11:18:30','2021-09-17 11:18:30'),('0c130ac3559c9588d79d9cb1603a5cbab51c50871c24b2fcd29591f57dc5ee66a277f0e6ed306c8c',1,1,'authToken','[]',0,'2020-09-20 07:24:54','2020-09-20 07:24:54','2021-09-20 07:24:54'),('0caaef7f613b52aca6a43cc141a3d813629349c42dfcd2c85f4241393368acb58f9d58ed89b2609a',1,1,'authToken','[]',0,'2020-09-20 07:04:33','2020-09-20 07:04:33','2021-09-20 07:04:33'),('0cbadd819b006534529721a5118e783c95284a516f60c51a421f044924f39bbd2632deb9b081074f',1,1,'authToken','[]',1,'2020-09-16 13:56:03','2020-09-16 13:56:03','2021-09-16 13:56:03'),('0cc3b7353561cc3f97163f87db00a66f6700bc0a6155773451a1dfa803ebc82449dec50c4580b4ac',1,1,'authToken','[]',0,'2020-09-20 07:15:07','2020-09-20 07:15:07','2021-09-20 07:15:07'),('0d144f3ff6eadb6a5f177922d8f1b972d4d8e54edfff5afb20998118c277c0d91ae9d670e874ff5b',1,1,'authToken','[]',0,'2020-09-20 08:04:33','2020-09-20 08:04:33','2021-09-20 08:04:33'),('0d1a7ea630c1dc72a97dfa63e21068fb75ce2f305c4732c2cf7f1ab537c6a0892087271db9465555',1,1,'authToken','[]',1,'2020-09-16 17:12:16','2020-09-16 17:12:16','2021-09-16 17:12:16'),('0e3cdc6d9a59c3148b80460d76638be04600a6024d19fc98c7d8bdc1f5e173b547a750c6cb3debfb',1,1,'authToken','[]',1,'2020-09-16 17:29:06','2020-09-16 17:29:06','2021-09-16 17:29:06'),('0e3f7992d04cfb7d2af059a5b597dd9c28b7057bbadc0b68df9d463a76d04f957e4e15b890da9af0',1,1,'authToken','[]',1,'2020-09-16 16:34:44','2020-09-16 16:34:44','2021-09-16 16:34:44'),('0e4adcd09427d59ef16cd07e13c652706e574a04390077e060be76eb3172c6cbbf8cc35d818a1dc5',1,1,'authToken','[]',1,'2020-09-16 13:39:04','2020-09-16 13:39:04','2021-09-16 13:39:04'),('0e6ba376d585e2ee4f800818483876a5c6f97cf33bae2000c660cd784bf6ada01113e3eb7b4b3c5d',8,1,'authToken','[]',0,'2020-09-20 10:25:47','2020-09-20 10:25:47','2021-09-20 10:25:47'),('0ee3e861e1bc61dcd78ec833417afa47dd929ff92679fd8999fd055b1ca76f2afc774617688df8a0',1,1,'authToken','[]',1,'2020-09-16 16:49:09','2020-09-16 16:49:09','2021-09-16 16:49:09'),('0f72561150d47dff607b5565c10c08de2373cbc202102666ad77974bf744ae08887da03855507451',1,1,'authToken','[]',0,'2020-09-20 08:01:17','2020-09-20 08:01:17','2021-09-20 08:01:17'),('0f72b4d6a02d90ee9152a7e2ed26f8f792b2e73388a87bc6b88d647a6f0b6b8896041d68be47a9e3',1,1,'authToken','[]',1,'2020-09-16 12:10:11','2020-09-16 12:10:11','2021-09-16 12:10:11'),('101b13072e3f4a8929a5963229aa13fa2b7208c49ae67679e772903372735061b7f6d108c853d742',1,1,'authToken','[]',1,'2020-09-16 17:12:16','2020-09-16 17:12:16','2021-09-16 17:12:16'),('1165d946e0b6f0c2ba96a220d3a9debfa0e5eba78bbc0853a8e2c77242c70dba1c12f54e0e61afb3',1,1,'authToken','[]',1,'2020-09-16 17:28:59','2020-09-16 17:28:59','2021-09-16 17:28:59'),('134d1940fbde2d651d7724c4ffd3bd96761fd8b0bd575f549aa2091538575ba28ac6e3f583c575fd',1,1,'authToken','[]',1,'2020-09-16 17:29:00','2020-09-16 17:29:00','2021-09-16 17:29:00'),('139a065f8261ced9771c362657acea9528b90c49673e720659f691fb1663409c2523e932e9dc57bd',1,1,'authToken','[]',0,'2020-09-20 07:17:49','2020-09-20 07:17:49','2021-09-20 07:17:49'),('13b509f552867ea272a9f856a8a391136633e1ed144da01458d0ed56df42acb23073dc1b87f84cc9',1,1,'authToken','[]',1,'2020-09-16 12:10:08','2020-09-16 12:10:08','2021-09-16 12:10:08'),('152586eb8e461d0ef2946b84d3390a5844dd09a7ee3d268e46b7107564ce741d2d329ef747ed5c2f',1,1,'authToken','[]',0,'2020-09-20 07:15:31','2020-09-20 07:15:31','2021-09-20 07:15:31'),('17ca9f2ffbe585e5f8054d55782383db883a1e5e0fe13aa0d656cb4441f3e854d70f7ac61c2222ba',1,1,'authToken','[]',1,'2020-09-16 16:34:31','2020-09-16 16:34:31','2021-09-16 16:34:31'),('1811ab4f140aaa2f44ae4270a48f8786fd397d8fb8d873e0d2d5e78f5d3499a6e1549003bfde9ee8',1,1,'authToken','[]',1,'2020-09-16 13:54:29','2020-09-16 13:54:29','2021-09-16 13:54:29'),('1841cb118de0dfd4f14aba25d4367c8b777ab247139078b90adf517c0f17bf7f0f3d9ddcd55d70ea',1,1,'authToken','[]',1,'2020-09-16 16:34:31','2020-09-16 16:34:31','2021-09-16 16:34:31'),('18d04eefba7e767cdf85749b590c05352800d182fe15c8e5f170ad77566004d4d0a238f455f5844b',1,1,'authToken','[]',0,'2020-09-20 07:07:27','2020-09-20 07:07:27','2021-09-20 07:07:27'),('19e83c2a65794c47d77237001a9c37fc38d9aa9179ab8900e3fa0bd0c331297cd644aeb9f21c5487',1,1,'authToken','[]',0,'2020-09-20 10:09:53','2020-09-20 10:09:53','2021-09-20 10:09:53'),('19ef8cbcbe5e5646edc477fe03153ffb694b24c8a3efd7175793d3bf75d0cdab3658df30c79e1169',1,1,'authToken','[]',1,'2020-09-16 16:49:06','2020-09-16 16:49:06','2021-09-16 16:49:06'),('1a9c0bdbe2789a7c1fab3ce59dd9fb1773a3b34b92823e506e4527450986dbbf9307d5264cf5b2ca',1,1,'authToken','[]',1,'2020-09-16 13:57:09','2020-09-16 13:57:09','2021-09-16 13:57:09'),('1c781c18b574460a7332872cfe883f4eec51ea6fa38f1c9c57d622a03df865624e073b5654a063c0',9,1,'authToken','[]',0,'2020-09-20 10:50:50','2020-09-20 10:50:50','2021-09-20 10:50:50'),('1ccc0d06eb40748878f1655aded0dff0319954ab3ffa454822db86eebb075495e7febc028286eb81',1,1,'authToken','[]',0,'2020-09-20 07:15:41','2020-09-20 07:15:41','2021-09-20 07:15:41'),('1d468e91e8548478f8b83c151ffade33037ce931d350122986570cc9c2694f66428a4f59b5b6ce4f',1,1,'authToken','[]',1,'2020-09-16 16:43:54','2020-09-16 16:43:54','2021-09-16 16:43:54'),('1d58a29766daa4261f71298997ab7d0e11ee51f297e350432fb36af3da998a35f427633600ada29a',1,1,'authToken','[]',1,'2020-09-16 17:19:41','2020-09-16 17:19:41','2021-09-16 17:19:41'),('1db2f672bdf91370fddc29e734f0bf6e6b2f35357f9215c1c70326e5e6ab0b2d9ffd7ab9d42d2fc7',1,1,'authToken','[]',0,'2020-09-18 04:29:02','2020-09-18 04:29:02','2021-09-18 04:29:02'),('1df2b17d486470a2c9bd965e55ad1f705288f2517c52fdaeff7c403d83fece88928b1c04f6c16c48',1,1,'authToken','[]',0,'2020-09-18 04:29:06','2020-09-18 04:29:06','2021-09-18 04:29:06'),('1e45b8e084576a53a9d8e78fd198a74b34872aa80d17267a569e22fb552d9ac27cc0d11a78eaf4d0',1,1,'authToken','[]',1,'2020-09-17 01:43:08','2020-09-17 01:43:08','2021-09-17 01:43:08'),('1f0bb54dc3856b7adc39ad91f4bc3100a64556aff17ea68b247e47aac60458c164ff1b725f7e4844',1,1,'authToken','[]',1,'2020-09-16 17:26:54','2020-09-16 17:26:54','2021-09-16 17:26:54'),('1f13453f353a18227778be2dc4261f02ebfcff1db8b02cb7c904efbf21b11de09ad3a2aa62b7ca9a',1,1,'authToken','[]',1,'2020-09-16 17:30:43','2020-09-16 17:30:43','2021-09-16 17:30:43'),('1f44d0bcce0a64c6461e9c1b6a0ba730b012cf34726a463db3b87f7d7d59f8160ccdf1e31d2920ca',1,1,'authToken','[]',1,'2020-09-16 17:28:48','2020-09-16 17:28:48','2021-09-16 17:28:48'),('1ff0629269bf549be75c66c337a8cc67080369b6338df2a167b9871aa613c007bdcb8e52a24ab353',1,1,'authToken','[]',0,'2020-09-20 06:59:48','2020-09-20 06:59:48','2021-09-20 06:59:48'),('202a4f13a9e78265c239ea20223a86de2e023f0536dd1a13052f1a7d2f70d8c0017e768005ae4165',1,1,'authToken','[]',0,'2020-09-20 07:09:48','2020-09-20 07:09:48','2021-09-20 07:09:48'),('202b95b2e855a0204bdab929418fd6e964f177fac7317fcb9b0589b5180f07e80f6219725faa25ed',1,1,'authToken','[]',1,'2020-09-16 17:16:10','2020-09-16 17:16:10','2021-09-16 17:16:10'),('202e6189fe30abf94f216b532dec345536765da952cfe711366bd5d750892af8c1b65e844cfa09f5',1,1,'authToken','[]',0,'2020-09-20 07:17:08','2020-09-20 07:17:08','2021-09-20 07:17:08'),('205fc2714421fbc4db7113e9fa6044558ff435a8a3906073a95c0a7c24f0b5d30b20a24d0e131956',1,1,'authToken','[]',1,'2020-09-16 17:17:40','2020-09-16 17:17:40','2021-09-16 17:17:40'),('20856233b2f76ac553a085b9bfecd7a02b61dc4cadc08c0e421e1952267636ca1b93f721006c8cb3',1,1,'authToken','[]',0,'2020-09-20 07:31:26','2020-09-20 07:31:26','2021-09-20 07:31:26'),('20bfd104817265dd678c2759371012aebf0b0e5bcaf3ed1cd603d1e929e6e830330c94ec92aa9777',1,1,'authToken','[]',1,'2020-09-17 01:55:43','2020-09-17 01:55:43','2021-09-17 01:55:43'),('20e89a083cd526f2979cd96e222eeef6f7d0d95fc8077b7f3706022fcb1b7a7fc6217f6178b03bc3',1,1,'authToken','[]',0,'2020-09-18 13:38:24','2020-09-18 13:38:24','2021-09-18 13:38:24'),('213b4c39cd8966b17e3edda04700fc454106b3ff562b6d896e467512db3e8e167c0100c56a5779b8',1,1,'authToken','[]',0,'2020-09-17 11:18:27','2020-09-17 11:18:27','2021-09-17 11:18:27'),('218268eef13689948a6cdafcc15b4c0296aed6f3b9fab0ef320d6a1a5e9899e85347e525292a2f4a',1,1,'authToken','[]',1,'2020-09-16 13:35:02','2020-09-16 13:35:02','2021-09-16 13:35:02'),('21f6547a94888f73f19dde9298a4baa1aa57a0086abfe3128cae4fcefb988a53c6e63cbb29228a47',1,1,'authToken','[]',1,'2020-09-16 15:58:09','2020-09-16 15:58:09','2021-09-16 15:58:09'),('21fc8328cbdd23ecef11c6cf73e6f4c24a5666984a308032a1838a9f09b329fe7cbfd087065e8c5e',1,1,'authToken','[]',1,'2020-09-16 17:33:23','2020-09-16 17:33:23','2021-09-16 17:33:23'),('2219c6577cc25c41cf9326621ee0d1d85a94a5d5fdb705c708a17981dacf876a0b0ffa69b2afd128',9,1,'authToken','[]',0,'2020-09-20 10:58:49','2020-09-20 10:58:49','2021-09-20 10:58:49'),('22a5652462594da1f8fb5ab840e52d9359f0f7daf00cbd9a4be8cea0975ad14cd3eedf4c25555ba8',1,1,'authToken','[]',0,'2020-09-17 11:18:25','2020-09-17 11:18:25','2021-09-17 11:18:25'),('22c5cb51d7fcd1fa63a843d0ccaefbcaeeaf4dd59d2ca14bb4ef03aece9c5f299342df10ee8801b3',1,1,'authToken','[]',0,'2020-09-18 04:29:03','2020-09-18 04:29:03','2021-09-18 04:29:03'),('22d3a3da7c353a5b42e254dde5b895eeba97e8c2acded974fa2a944262ad7b21db2d49c5d7c78bfc',1,1,'authToken','[]',0,'2020-09-20 08:39:14','2020-09-20 08:39:14','2021-09-20 08:39:14'),('22f954834b2ad4c274e15a86ebd7b4ff13bb78552e72f61d0c4859ab1c7d61859b9dddfae08085ef',1,1,'authToken','[]',0,'2020-09-20 08:01:42','2020-09-20 08:01:42','2021-09-20 08:01:42'),('239bea2f347ac0ecc623e07a880824ae1291a30e9c196f386f843f5df6ffdfa09a8c59995c223108',1,1,'authToken','[]',1,'2020-09-16 17:13:05','2020-09-16 17:13:05','2021-09-16 17:13:05'),('2462481b06c8016e380ac530bb31847ac835b2dac85b1de107c89895a537679b28d9cf98aea380fa',1,1,'authToken','[]',0,'2020-09-18 13:25:01','2020-09-18 13:25:01','2021-09-18 13:25:01'),('24c4893a9bb74542210f7e0a29fe602130beedb2741a3534a378b5f7dcd0216b1b583abc68bc1d81',1,1,'authToken','[]',0,'2020-09-18 13:35:09','2020-09-18 13:35:09','2021-09-18 13:35:09'),('24e01b939f66ee42718f7321ba8288f1fb1537ffdb49d1d1d814bee35a5f99ef0ec134d5e6b2baaf',1,1,'authToken','[]',0,'2020-09-18 01:01:56','2020-09-18 01:01:56','2021-09-18 01:01:56'),('24f3935f2c04b93852cba5f991cf41b4b7d6c2819d502a9eb0b7aea00fbc03114634b928fd3d6fb2',1,1,'authToken','[]',0,'2020-09-20 07:17:53','2020-09-20 07:17:53','2021-09-20 07:17:53'),('251ce13365e109ca550c0010371727c6bc20252828b47766d8a7b0f7c1c0066d073b49abdf8dd44c',1,1,'authToken','[]',1,'2020-09-16 17:15:00','2020-09-16 17:15:00','2021-09-16 17:15:00'),('266d36f0749c03ca571956086ab29e816dd166a364405c8281f9ad631962420e8b119fb096486ae5',1,1,'authToken','[]',1,'2020-09-16 17:12:18','2020-09-16 17:12:18','2021-09-16 17:12:18'),('2707e6f5ec65d4adf6eaf67b4012014bedfb7319ed6ad62659c0e95a5f6cf68f5900d144efd08820',1,1,'authToken','[]',1,'2020-09-16 12:10:46','2020-09-16 12:10:46','2021-09-16 12:10:46'),('287c4c220c5b66418c55079d07a09b2ebcb07bef5a8bb96dc9f7e433a7ff9e1268c6de65f125f0eb',1,1,'authToken','[]',0,'2020-09-20 07:41:14','2020-09-20 07:41:14','2021-09-20 07:41:14'),('28bcea3f6c909d6f771053437bb7515587c43ba273cb3ba859b8554d8f8dfa16a0b2973f73f1a764',1,1,'authToken','[]',1,'2020-09-17 05:26:31','2020-09-17 05:26:31','2021-09-17 05:26:31'),('297b0486494b24a3983f0c0e516f363fec7ea95958a405a8b5f21ef70a5bfc5ab8d637954c80a018',9,1,'authToken','[]',0,'2020-09-20 10:42:59','2020-09-20 10:42:59','2021-09-20 10:42:59'),('29ed35ba935a45eb0f8b6e467921962486fa68fb3706a0e30e69633fd81c52b0a5f660f564a2a179',1,1,'authToken','[]',1,'2020-09-16 17:14:18','2020-09-16 17:14:18','2021-09-16 17:14:18'),('2a2c345601033288105cb2de56c65aa43d1467a8ac96b94fa8966da6fbfbab648ff39b6c78f9a281',1,1,'authToken','[]',0,'2020-09-20 07:09:48','2020-09-20 07:09:48','2021-09-20 07:09:48'),('2a7719b6f316edf7280b895ad91cf27d0c51989c6486f8486a4d09412988db9fe10b0fc1a211db12',1,1,'authToken','[]',0,'2020-09-20 07:11:58','2020-09-20 07:11:58','2021-09-20 07:11:58'),('2b1f4b7ac07fc5505488f5ecf062fbe8a99aa7b3a789f1524dcde8b4b47c8f73b1c0f6b3d5745292',1,1,'authToken','[]',0,'2020-09-20 09:43:02','2020-09-20 09:43:02','2021-09-20 09:43:02'),('2c381eb1960404eadc9e8821dec8154e78c17890ac6b79a9179d30dc99af0fa4e608d4ffed4dde33',1,1,'authToken','[]',1,'2020-09-16 13:34:01','2020-09-16 13:34:01','2021-09-16 13:34:01'),('2d951bed6629c543867c28016b4e914953ae5e1a17d03b3df5ffe4372f51533158e05235818ce3e8',1,1,'authToken','[]',0,'2020-09-20 07:16:46','2020-09-20 07:16:46','2021-09-20 07:16:46'),('2df5a7f3dbc59285c663adf2088b47e4d66279cdbf6545ac5677c0723399aba4c0a5da6319dc2799',1,1,'authToken','[]',0,'2020-09-18 04:29:04','2020-09-18 04:29:04','2021-09-18 04:29:04'),('2e719d1140d8c94831f2b8dc9dca2c3541005bb00685fb5df54bd66e87c4be2498ea740e1a2de3ff',1,1,'authToken','[]',0,'2020-09-20 07:20:36','2020-09-20 07:20:36','2021-09-20 07:20:36'),('2e9744fa8d86778309b2270d2c54ba53c4af5fdc8f030e65af683096449644640b1ab6234b516004',1,1,'authToken','[]',0,'2020-09-20 08:19:30','2020-09-20 08:19:30','2021-09-20 08:19:30'),('2e996ec184a78a3cd366cdf971744e8d5977a17d6ec5ed0e8614b653ea93ebdf414151d09a68cc61',1,1,'authToken','[]',0,'2020-09-20 07:16:24','2020-09-20 07:16:24','2021-09-20 07:16:24'),('2eb08204822f38e1c5846dd780379ec584e17260fe7727425106129fd943ba72dd573c672c32df99',1,1,'authToken','[]',1,'2020-09-16 17:21:34','2020-09-16 17:21:34','2021-09-16 17:21:34'),('2eb0c9161774dbcf4ede367a2bd2840f0b6b9a88bd017c25a052164e3fa9e053e8c82c43b8fcdbf9',1,1,'authToken','[]',1,'2020-09-16 17:12:16','2020-09-16 17:12:16','2021-09-16 17:12:16'),('2eb4524fbd1d27c4f733570d8be8d3d0bb2329943d19326c47323831d9ee302f57a25124230cc06e',1,1,'authToken','[]',0,'2020-09-20 07:17:53','2020-09-20 07:17:53','2021-09-20 07:17:53'),('2ecadf750fcacba7b80d8e0b766c55e22183ef0c296999e0c3de6d419940e02a3713372edc2a1741',1,1,'authToken','[]',1,'2020-09-16 15:02:58','2020-09-16 15:02:58','2021-09-16 15:02:58'),('2ed892dbd7323e255d5a0975fada64b8810add66dd5839d0abc1c35fef06e6a69c30a99d6bebb099',1,1,'authToken','[]',1,'2020-09-17 11:18:18','2020-09-17 11:18:18','2021-09-17 11:18:18'),('2ed920e90f66b6a059ccbc206cf8c12108f1dc372710196019c91ca6125961f13bda3302a11c4138',1,1,'authToken','[]',1,'2020-09-16 14:47:04','2020-09-16 14:47:04','2021-09-16 14:47:04'),('2efa3e13b05ff9493e38b54c058cfc8dbb4ef8b584df71af406812a3ed084e392eb8cc25764b0576',1,1,'authToken','[]',0,'2020-09-18 12:25:21','2020-09-18 12:25:21','2021-09-18 12:25:21'),('2fbaa9adff403ff89534a9579016a0c2363fbe74672d797ad34339e5449cf9a969076c430d6c78c7',1,1,'authToken','[]',0,'2020-09-18 12:18:19','2020-09-18 12:18:19','2021-09-18 12:18:19'),('2fed6c8a60f1cb428834e46be5de5e0c56e6d71a01e015c0d648dfd2cf80468722a3c2557da5e256',9,1,'authToken','[]',0,'2020-09-20 10:42:58','2020-09-20 10:42:58','2021-09-20 10:42:58'),('30046231b025fbc847a77054c5ef1433c97ba5e8abe45ee5a7aa7d3d922c627968d47466c54b682b',1,1,'authToken','[]',1,'2020-09-16 13:48:33','2020-09-16 13:48:33','2021-09-16 13:48:33'),('3081a6325ff54f5651952b9ebeadff9c2b708e45dad1fc5dcb50dab41ffca6f72e0a2c3cff21bdeb',1,1,'authToken','[]',1,'2020-09-16 17:23:47','2020-09-16 17:23:47','2021-09-16 17:23:47'),('30b5c837e72201af6c2bd8a3c695217b26d6c93afbb3d3e419a301f00d43da85445a62b09845bcee',1,1,'authToken','[]',0,'2020-09-18 12:46:58','2020-09-18 12:46:58','2021-09-18 12:46:58'),('30cd69277840a645a95a1f1f056ecdf6da3552f60fedabb84050e20caec02d81c81bb2ead08f39cc',1,1,'authToken','[]',1,'2020-09-16 17:23:48','2020-09-16 17:23:48','2021-09-16 17:23:48'),('314ce6ba7353fa1ee99e01b5a85921285985b74933fb81036f550c1f0d1582d79e776c919227a340',1,1,'authToken','[]',1,'2020-09-16 17:34:12','2020-09-16 17:34:12','2021-09-16 17:34:12'),('3155f81ba05d7641e0e5510f117578cf89f68b9cdd28850fec1baaa826a73f436add7f6fa346660e',1,1,'authToken','[]',1,'2020-09-16 17:26:14','2020-09-16 17:26:14','2021-09-16 17:26:14'),('32167cc9d40fb188b860b449beaa6af4f76d076272086a2eef7bfdfa91bd96e816ed4e4e53a6cae7',1,1,'authToken','[]',0,'2020-09-17 11:19:01','2020-09-17 11:19:01','2021-09-17 11:19:01'),('32335a3ef357d0af7d64c33dafad036088080dc2160e316d69985a35393eae7180400b302f9c4293',1,1,'authToken','[]',1,'2020-09-16 17:18:13','2020-09-16 17:18:13','2021-09-16 17:18:13'),('328f0328b61e3dd5a12b91da3b5ad92bf84327c52d19f65b36ac32f9dc5584e0d9860117dc3bd219',1,1,'authToken','[]',1,'2020-09-17 01:54:42','2020-09-17 01:54:42','2021-09-17 01:54:42'),('340b7cb0d6985a0678334fe3d3010c764d0a582e0db8224babb26e3519cc6e914f44e749a4312b03',1,1,'authToken','[]',0,'2020-09-18 12:07:25','2020-09-18 12:07:25','2021-09-18 12:07:25'),('35cac9f35c05f9f38197ba0b9645ac4622f8ffa0c1deec07f005383ba0a72c59e511ffed4fbd551e',1,1,'authToken','[]',0,'2020-09-18 13:25:51','2020-09-18 13:25:51','2021-09-18 13:25:51'),('35d074bcf5257247d4ce2dcd36d0bae50c56e0edac5f11c99c2d64c5130b461f93ba2d0c56a059ae',1,1,'authToken','[]',0,'2020-09-18 14:16:27','2020-09-18 14:16:27','2021-09-18 14:16:27'),('3658bf21ade537f2e8c3b8d443907918afcb211369ca7f2116c9f2011d893470749b104d410a10d6',1,1,'authToken','[]',0,'2020-09-18 12:25:36','2020-09-18 12:25:36','2021-09-18 12:25:36'),('36996a08f32fb1ac26bd3888063d91afa70062ecc7982671bea5474f256e2f76bd80df724648eab0',1,1,'authToken','[]',1,'2020-09-16 13:44:19','2020-09-16 13:44:19','2021-09-16 13:44:19'),('375935527aa5ad761f24e7e7c58cc8c4eb14da3c7029f812a15d68919071a04d7360ab5ac11fb4d0',1,1,'authToken','[]',1,'2020-09-16 17:28:26','2020-09-16 17:28:26','2021-09-16 17:28:26'),('37b8f4d41eaac4582040059ea6cb573cb08089584e96a72093e7285d810353d261495d766291189a',1,1,'authToken','[]',0,'2020-09-17 11:18:25','2020-09-17 11:18:25','2021-09-17 11:18:25'),('39a0b3e7a0c09e2e1db3ad3ed4d6100d360132f7d29eba89d476d636f0a41f8c6843f8f5e776649c',1,1,'authToken','[]',0,'2020-09-20 06:53:39','2020-09-20 06:53:39','2021-09-20 06:53:39'),('3a7d16a47d83add329347feb04a3f58ea755eb3e092101f6a8871b585445a8d1162622a4266279d9',1,1,'authToken','[]',0,'2020-09-18 12:26:32','2020-09-18 12:26:32','2021-09-18 12:26:32'),('3ab5bfdb954c9224dcde94f9b1c3b8679aa962e8d72a258407aadd3c8e0ccf2502c75ac90e0fa4b5',1,1,'authToken','[]',1,'2020-09-17 01:46:52','2020-09-17 01:46:52','2021-09-17 01:46:52'),('3abc3224515e91b1be28c866b300e310b6b2a44bea29014552c07ba420fadfa9af283c6973c791d7',9,1,'authToken','[]',0,'2020-09-20 10:58:11','2020-09-20 10:58:11','2021-09-20 10:58:11'),('3acd7ef0eab8c903fa9f60b9f9a5d0d34f8a2f51ecfd29d7bf7a22288b3ffa87ac03a8658bf728f4',1,1,'authToken','[]',1,'2020-09-17 01:43:10','2020-09-17 01:43:10','2021-09-17 01:43:10'),('3ade6328f4f82057dc240e764225475a2038c13a749cde2509af4d57bff634b8464560ed93402ea9',1,1,'authToken','[]',0,'2020-09-20 07:12:54','2020-09-20 07:12:54','2021-09-20 07:12:54'),('3bef71884f383a1c8caba0739a5178d370e3b178c067d0e3ab4c4e37a16b23890041b633d403b515',1,1,'authToken','[]',1,'2020-09-16 17:12:11','2020-09-16 17:12:11','2021-09-16 17:12:11'),('3bf15751f60a972a8b713a246dfb843786366799747a5b31d7932bddf4eb4ecba8dd01dad9487ba7',1,1,'authToken','[]',1,'2020-09-17 01:45:34','2020-09-17 01:45:34','2021-09-17 01:45:34'),('3c9e3baad99fae5e781954ae525713c0d5e4ca5ead64220318af220073a9d680a7e53f926a4cb5cf',1,1,'authToken','[]',1,'2020-09-16 17:24:06','2020-09-16 17:24:06','2021-09-16 17:24:06'),('3cbdbbe4cfd88367cf216444fc28100cc02834406a62c686bfefc2fb4222044b0da7aecefaf5e365',1,1,'authToken','[]',1,'2020-09-16 13:58:24','2020-09-16 13:58:24','2021-09-16 13:58:24'),('3d33cbc8c50ffad646e14816da99963622acc90f778cf0e41636a5a44a858872ce5f36e1f402d729',9,1,'authToken','[]',0,'2020-09-20 10:58:11','2020-09-20 10:58:11','2021-09-20 10:58:11'),('3d48ad45e74525402e5c39e6e316b67c77d3ba6c7cbe79fe413e373cdda04dfc2a18f48dde3a9ea5',1,1,'authToken','[]',1,'2020-09-16 12:11:29','2020-09-16 12:11:29','2021-09-16 12:11:29'),('3d68326697bb5068d84e712e7ebacaeeed90fd61d4a7b67d7a4e31c5bcc03f9ad557d9e5addfc04f',9,1,'authToken','[]',0,'2020-09-20 11:12:08','2020-09-20 11:12:08','2021-09-20 11:12:08'),('3e232cfda1fe140967da5b5c5e48f2d713795ab9e12f9eab0d16ca3968731a0e650de883ea3fea9d',1,1,'authToken','[]',1,'2020-09-16 17:35:51','2020-09-16 17:35:51','2021-09-16 17:35:51'),('3edd6db13b61c04d0decbbc9d9d477a17441b323b9c693101b3ff0039105ce39a6e660a0957ff6b4',1,1,'authToken','[]',0,'2020-09-18 01:01:47','2020-09-18 01:01:47','2021-09-18 01:01:47'),('3f532b50ccebea30e4aad9c627a3e234da5ecfea297236834fbe7822fde42ce69f05cd689ad19612',1,1,'authToken','[]',1,'2020-09-16 17:13:04','2020-09-16 17:13:04','2021-09-16 17:13:04'),('3fe47fd3273b801fc9f0a40a6eed04700d3ddc1593737be52ea1e2724693cdbaea8e3109d84ca649',1,1,'authToken','[]',0,'2020-09-20 07:16:56','2020-09-20 07:16:56','2021-09-20 07:16:56'),('3fe65d8af70c08c1c06bf157e2e0013d56b4e76ebe08cd2c06a4ad721da8c5096bda680dc94ea652',9,1,'authToken','[]',0,'2020-09-20 11:01:08','2020-09-20 11:01:08','2021-09-20 11:01:08'),('4059c8f9a5e9254cd66a85bf979d411ac23a47f5e6a67b8aa21f0a148f88fb8d54936d40d0e0bb9a',1,1,'authToken','[]',1,'2020-09-16 17:12:19','2020-09-16 17:12:19','2021-09-16 17:12:19'),('40bc679f41e77f8db7d13999602a3a5a39201cec89798c9152828293a6c229c04289e04d9f68a85d',1,1,'authToken','[]',0,'2020-09-20 07:15:28','2020-09-20 07:15:28','2021-09-20 07:15:28'),('417a7d3fe03ea3d8a3c873eeacbd17b60e721d27ae2d60ec6f7e07c41096baa863655b322e84537d',1,1,'authToken','[]',0,'2020-09-20 06:59:39','2020-09-20 06:59:39','2021-09-20 06:59:39'),('41831295e34020fd25d234d116b93cd1fa310c844f64aadf357db5b6a892243220f8d96f535e2597',1,1,'authToken','[]',1,'2020-09-16 15:03:12','2020-09-16 15:03:12','2021-09-16 15:03:12'),('425207a191045b1e77d35d8b138efcad830b34871e1f634490718158b23a727c18b115b3640f6099',1,1,'authToken','[]',0,'2020-09-20 08:13:06','2020-09-20 08:13:06','2021-09-20 08:13:06'),('447df5ce6d087b946a8410f09df328c9efdee68bc1ef056c9d606a71e81398933ae9cc166819d877',1,1,'authToken','[]',0,'2020-09-18 12:47:17','2020-09-18 12:47:17','2021-09-18 12:47:17'),('44ecefbbbc5f54500a2f9f15ba25b53341256cd4790296038af4e837e0e152d7f47dd747112b06de',1,1,'authToken','[]',1,'2020-09-16 14:47:04','2020-09-16 14:47:04','2021-09-16 14:47:04'),('4505a8f19410503b33535e40955b1182c9290e343ee0d8809582be1904f2898d6e5e0f74dfc82776',1,1,'authToken','[]',1,'2020-09-16 17:31:00','2020-09-16 17:31:00','2021-09-16 17:31:00'),('45203dcc865d0a4c50baea21f49239ca3def46a69d77595be1ecd52eb5db05adc00896ea623a5d44',1,1,'authToken','[]',0,'2020-09-20 07:15:41','2020-09-20 07:15:41','2021-09-20 07:15:41'),('464b64f164be4dae382f99b469d51af886c51d4429a58f8aebb9d1192626c23144c5fc9552a78173',1,1,'authToken','[]',1,'2020-09-16 13:47:18','2020-09-16 13:47:18','2021-09-16 13:47:18'),('4739b3d7cdb650c52b6e739fbae7058ebe064a734538a86a11d30fd35322eb8034a10eff3c72da9a',1,1,'authToken','[]',0,'2020-09-20 07:09:24','2020-09-20 07:09:24','2021-09-20 07:09:24'),('4764b82e27798b3f127f3cce84fe481d8db5a77f939834c3c0cde31743d4266f2da8d359002be364',1,1,'authToken','[]',0,'2020-09-20 07:25:02','2020-09-20 07:25:02','2021-09-20 07:25:02'),('482c8af6a13947378b0ef2acb684774121a11c638cb4ad1c7ab490f551695d5ed675835531bcc6ad',1,1,'authToken','[]',0,'2020-09-20 07:13:53','2020-09-20 07:13:53','2021-09-20 07:13:53'),('48a4b12ca32d0f8ebe591d4501ff50e071af06642037d4e3a6503fdb27c1aee77ba3fe5bb4a54d03',1,1,'authToken','[]',1,'2020-09-16 13:39:04','2020-09-16 13:39:04','2021-09-16 13:39:04'),('48db1c7669502e329b3e2979b4f300bf862b952c835e1eee5d8ad1c4df82da9618117caeea8ca816',1,1,'authToken','[]',1,'2020-09-16 13:35:02','2020-09-16 13:35:02','2021-09-16 13:35:02'),('4957dd4173221734dde55345b8d2cf2a42f2c2747343c3a5f784553bf6d80ee81ecea426f4218261',1,1,'authToken','[]',0,'2020-09-20 08:23:49','2020-09-20 08:23:49','2021-09-20 08:23:49'),('497c24c8f4e39866c2feddece60b7760a2feca57fefb2e9c2c9180013fba54463f66f631ee783199',1,1,'authToken','[]',1,'2020-09-16 17:14:11','2020-09-16 17:14:11','2021-09-16 17:14:11'),('4a081f882671d05a9d2f636734b33f5d108cc8a5776f4f9e9f5b923aadfc98802de544c080df327f',1,1,'authToken','[]',0,'2020-09-20 08:09:34','2020-09-20 08:09:34','2021-09-20 08:09:34'),('4aa75ac147623ee5e6c26400cdc884fcd7972c0d1c3cf083fed92e395f0532ac67688c958c7f5ef4',1,1,'authToken','[]',0,'2020-09-18 04:29:00','2020-09-18 04:29:00','2021-09-18 04:29:00'),('4aac3d6096af53c6451b4f3f85ebe2f6a4bb18c0773c2ddee19fa0c442d9e3db5264f2f55554c7a0',1,1,'authToken','[]',0,'2020-09-20 07:58:32','2020-09-20 07:58:32','2021-09-20 07:58:32'),('4ae5fa821586017eaa088947077b0060c7d9e66bea2424a4a62083df74c3d6c458afc1caf092ab12',1,1,'authToken','[]',0,'2020-09-20 08:03:15','2020-09-20 08:03:15','2021-09-20 08:03:15'),('4c9ec28afab7a5c660d28d667c02fc03ad03ea798d9ae1cc8cc889f60ffaef5745e9b82abc3bd1d9',1,1,'authToken','[]',0,'2020-09-18 13:27:44','2020-09-18 13:27:44','2021-09-18 13:27:44'),('4d36a52761bbd1b805cf76c5ba2fa66cb5bbb777efc44ad6bf94a7c345d3da247758fb48ad11f060',1,1,'authToken','[]',1,'2020-09-16 13:48:59','2020-09-16 13:48:59','2021-09-16 13:48:59'),('4eef167a2fb4f5464fe96cda6043aeb66f122419c819f0de32705bdb43c3e3dfe75a363943809e8c',1,1,'authToken','[]',1,'2020-09-16 16:43:53','2020-09-16 16:43:53','2021-09-16 16:43:53'),('4f48774b1812b9ff0e86aacbdb9ceb8af2799a125b2995a1a1ff198c3dfc5a24e60bde85332ef753',1,1,'authToken','[]',1,'2020-09-17 01:45:39','2020-09-17 01:45:39','2021-09-17 01:45:39'),('4f89637b53db4ccf08fb66b84eacf7d834f67e172b9f5ea8d21432acc1f5d5e93321767e6a47dc1a',1,1,'authToken','[]',0,'2020-09-18 01:01:53','2020-09-18 01:01:53','2021-09-18 01:01:53'),('50ab649099dce46e0ba878a11991d26b52d128856a1963ab073d7624928d9d74451f1136fd4f82d2',1,1,'authToken','[]',0,'2020-09-20 07:25:47','2020-09-20 07:25:47','2021-09-20 07:25:47'),('5139dd129777fb935959dfcf21ab5aca29ef9faa3ae590b8101a03423aad376463e5bcfc77145499',1,1,'authToken','[]',1,'2020-09-16 17:28:41','2020-09-16 17:28:41','2021-09-16 17:28:41'),('5293bc6a20b14f0f2a2c2f33c45072ab9a9eb66f5a33fbbb7c8f1ec8db91c17c4a19a37d8719958d',1,1,'authToken','[]',0,'2020-09-18 04:29:00','2020-09-18 04:29:00','2021-09-18 04:29:00'),('52b5e29b5c66f949aee50edf175e4d4627bf0cb4c7db1e6c92ee46543c14c2ff104784e2c353c60f',9,1,'authToken','[]',0,'2020-09-20 10:51:52','2020-09-20 10:51:52','2021-09-20 10:51:52'),('53a6bc81e1859ad31d3dd1ba1893971184e71082d3015b610d9b9f2e3799783853813ca30ef737ff',1,1,'authToken','[]',0,'2020-09-20 07:13:07','2020-09-20 07:13:07','2021-09-20 07:13:07'),('53e8729b2fea38e1222f31ed83085fee682d19c9bb0ea0490a986ecd25f06b7636426ff7df3df6e7',1,1,'authToken','[]',0,'2020-09-20 07:01:58','2020-09-20 07:01:58','2021-09-20 07:01:58'),('53fa19f4613d1f83a6157c9df8d282040d252baccae04093ff910dc3f12dd9c442d60bf5620dbde1',1,1,'authToken','[]',1,'2020-09-16 14:47:03','2020-09-16 14:47:03','2021-09-16 14:47:03'),('53fc7de937fb10afeeedf9a6b168aae1b138d274b94ecd0a984a539614f0e77dec707aa404c46d88',1,1,'authToken','[]',1,'2020-09-16 17:36:32','2020-09-16 17:36:32','2021-09-16 17:36:32'),('543e9a6f9a39ad17328bcbbae4aa1b07fdbb4e2a4d8d38ef833cf26259e23b8cfb10143e5072ba0f',1,1,'authToken','[]',1,'2020-09-16 17:12:18','2020-09-16 17:12:18','2021-09-16 17:12:18'),('544ad382b274d15b97ffa879a35237d4e04df39e442be5fe95484ff152eba570e4ee099912959be8',1,1,'authToken','[]',0,'2020-09-18 04:29:06','2020-09-18 04:29:06','2021-09-18 04:29:06'),('5550139653d6b45977b8460bb754a91695c337f486698f46ba04ebd4cab643ad8ee7284d1d82d379',1,1,'authToken','[]',1,'2020-09-16 17:14:24','2020-09-16 17:14:24','2021-09-16 17:14:24'),('55d368be7486c11b14e7ecd55b30a4044f629b10f8caf619dc2275a6a1deeb86f283c56c9c8e7fd1',1,1,'authToken','[]',0,'2020-09-20 06:53:39','2020-09-20 06:53:39','2021-09-20 06:53:39'),('55eaf83c57eff4f1e0b9accfd16c7c1f8b5293c428b07d1a5a777ec530c5bead6e19ca60abd0e4d1',1,1,'authToken','[]',0,'2020-09-18 12:07:10','2020-09-18 12:07:10','2021-09-18 12:07:10'),('5679ce04b44170f821207b49d58b7963bcff0394dd7e929be76a6c07cc4174e9667c42bc0838b669',1,1,'authToken','[]',0,'2020-09-20 10:09:53','2020-09-20 10:09:53','2021-09-20 10:09:53'),('5730c144321c458eb08334479e2973f9d6c7a378ed0fac117c839f4de5319714f934a3ad1b2db67b',1,1,'authToken','[]',0,'2020-09-20 08:08:53','2020-09-20 08:08:53','2021-09-20 08:08:53'),('57c1b5c6c6a9ac9911f20f422107b4f6c865ea48ad02f682d7b4f54bfc7c58338849df3c36861141',1,1,'authToken','[]',1,'2020-09-16 15:58:08','2020-09-16 15:58:08','2021-09-16 15:58:08'),('57f3651190c08dcb570fe4dab2a16252279f22b53ea895cc45fe9aee9ce984a3d20aa3c0fd92c653',1,1,'authToken','[]',0,'2020-09-18 12:11:06','2020-09-18 12:11:06','2021-09-18 12:11:06'),('5b07d0782571523b3f71d0a2ab98cbd8fdf3911426f8fe85ba60889dc218ea935707c99f7aa55bb2',1,1,'authToken','[]',0,'2020-09-18 12:07:11','2020-09-18 12:07:11','2021-09-18 12:07:11'),('5b69f4c921b5fb578926527ccdd4caf7680f0b2fac21884acbf6f3f695ed5a79b3d40f279534ac19',1,1,'authToken','[]',0,'2020-09-20 07:20:36','2020-09-20 07:20:36','2021-09-20 07:20:36'),('5b93fec2ec2186ce6f4842c2885298b7166042c64bbeb34af8e768e7b54dbf098f8edda8f04a4e95',1,1,'authToken','[]',0,'2020-09-20 07:31:28','2020-09-20 07:31:28','2021-09-20 07:31:28'),('5c8ad47a6bd99e52b536466f7966599bbba87e1a8a47c2357aa873c5394fe6e9e674e1e0c384fe31',1,1,'authToken','[]',0,'2020-09-20 07:16:22','2020-09-20 07:16:22','2021-09-20 07:16:22'),('5ce660de769b88748355e1f3132c292164bcf940bce341ea0ed48fe4306d5dab602a934872f5a480',1,1,'authToken','[]',1,'2020-09-16 17:28:59','2020-09-16 17:28:59','2021-09-16 17:28:59'),('5d2c5c791c226dd19b41beaa4930f9ade964c0af9b46f1dd92b54329dcafd380076ecc0a9811d221',1,1,'authToken','[]',0,'2020-09-20 07:16:31','2020-09-20 07:16:31','2021-09-20 07:16:31'),('5d409ee396e2aaaa2b6ae2517e5e7f235112f12dfd4377af36d689f4fa8ba67b2b4fe5ebc00964ad',1,1,'authToken','[]',0,'2020-09-20 07:41:30','2020-09-20 07:41:30','2021-09-20 07:41:30'),('5d64f81e92dfd6e8a93f1bf6dd605244fb8b6367c46daf99d6fdea57ba1dc5d54128aa4864ff708f',1,1,'authToken','[]',0,'2020-09-20 08:03:15','2020-09-20 08:03:15','2021-09-20 08:03:15'),('5db7cb1ad191fe457a30020a6743ef9f606b174fd78d8f12502a72b8c28c212e9fb86160866545c9',1,1,'authToken','[]',0,'2020-09-20 08:01:17','2020-09-20 08:01:17','2021-09-20 08:01:17'),('5e8fdcc7c82c9bbf6123c3a57a49b0a97048917667710400bb78be15e62dd12252bfa8e14c24d71d',1,1,'authToken','[]',0,'2020-09-20 08:08:39','2020-09-20 08:08:39','2021-09-20 08:08:39'),('5f47d40c646376ae9711eca93fc341246355cfb47cb6ee028704a8859ecd35fc29811e2fe749b3dd',1,1,'authToken','[]',1,'2020-09-16 13:48:52','2020-09-16 13:48:52','2021-09-16 13:48:52'),('5f7cecd81ea596ca3803f8285cfd3b3a3a6974c523ffae2f9ac6f31f43317a2614def568f3840d1d',1,1,'authToken','[]',1,'2020-09-16 13:54:14','2020-09-16 13:54:14','2021-09-16 13:54:14'),('5ff465d5de0b09a7d6f81c2534dda83f5d28ef40d566eb78e2f4bafbfc841ce0cdaea221fd5c4f55',1,1,'authToken','[]',1,'2020-09-16 16:37:13','2020-09-16 16:37:13','2021-09-16 16:37:13'),('6057f0785ebcf81f107e3f55866d5816a852469648f251224ac88a656bfbbc9b48d4291c87d356fe',1,1,'authToken','[]',0,'2020-09-20 07:31:28','2020-09-20 07:31:28','2021-09-20 07:31:28'),('62117bab80ed3531661ee07ef2acd84519b02b66f97db1f02fb34201ee2a6652747c993e40919c61',1,1,'authToken','[]',0,'2020-09-17 13:26:09','2020-09-17 13:26:09','2021-09-17 13:26:09'),('62badd571c848757ddd56c9c76a0c03caf5218f36a5c9846cfe5a7307067102878c3d899e314118c',1,1,'authToken','[]',0,'2020-09-20 08:13:21','2020-09-20 08:13:21','2021-09-20 08:13:21'),('6325c85f6ec15e279db6e5256f35fd0d5be3c5b87e114573f4b1d19cece0608cb79448cf77de01aa',9,1,'authToken','[]',0,'2020-09-20 11:01:08','2020-09-20 11:01:08','2021-09-20 11:01:08'),('64c6215e3b75335e7705f1bd177f20113663fcec4c695ae46160513dde4189c0ed94100fd889b740',1,1,'authToken','[]',0,'2020-09-20 07:31:02','2020-09-20 07:31:02','2021-09-20 07:31:02'),('64d412a736dc96eb15bd96018a0a08dc2f86299c4a5273839176afd1309b7557c82552b6a6d025ac',1,1,'authToken','[]',0,'2020-09-17 13:26:13','2020-09-17 13:26:13','2021-09-17 13:26:13'),('64f6dd5a62fac57f1a8862ed92a5551191c93a9c36950abfa6e2c99f11b09b2a672ea117f48beff0',1,1,'authToken','[]',0,'2020-09-20 08:19:30','2020-09-20 08:19:30','2021-09-20 08:19:30'),('650a1c4e91dbee5ef06c06d69f73cf1d86f1925c77e712826f9d8b4644fd9b97efcdf0901f32c2d2',1,1,'authToken','[]',0,'2020-09-18 01:01:58','2020-09-18 01:01:58','2021-09-18 01:01:58'),('651e3b62b0f6fb829c4ae3f9c5db5686d223b1621f66954411ced06c8523ecee6aad7565abb89813',1,1,'authToken','[]',0,'2020-09-18 12:10:03','2020-09-18 12:10:03','2021-09-18 12:10:03'),('6577213bda261d117ac5e2cb9ae23fee8b9e33ecc7e8c19b0773a988d596d2203622684b19577cc6',8,1,'authToken','[]',0,'2020-09-20 10:25:47','2020-09-20 10:25:47','2021-09-20 10:25:47'),('658c655e38a8c5e6ab1cf102aaaa8caee9b7ac7e8db2816073a3cb3c42c559aee258a726b439c813',1,1,'authToken','[]',0,'2020-09-20 07:20:47','2020-09-20 07:20:47','2021-09-20 07:20:47'),('674a0280ec1a21b9c57bc7dc902f682fe33c6ac05cade2d4cb04c890844e3a9905ceb8f1cc6c0b97',1,1,'authToken','[]',0,'2020-09-20 08:31:56','2020-09-20 08:31:56','2021-09-20 08:31:56'),('680bd37207c38d6d4be5c18347dd235ae6768da554aac9f5e56c660b08ab991fe484b40537c286f8',1,1,'authToken','[]',0,'2020-09-20 08:23:55','2020-09-20 08:23:55','2021-09-20 08:23:55'),('684383be350d4d49f1b08bdb5e3fed103ba45f48781127d282b6e07479852a9e0dafaa4757d521d2',1,1,'authToken','[]',0,'2020-09-20 07:31:43','2020-09-20 07:31:43','2021-09-20 07:31:43'),('68acdd250f2dab87d36ad97a978d6ad9fc42efa83c5bd94c3434e9ddbcbfe7c9f702e34192088301',1,1,'authToken','[]',1,'2020-09-16 17:16:12','2020-09-16 17:16:12','2021-09-16 17:16:12'),('68cdcfb89d2b3a1ff4c52c7faa0cace90be0a1c42cedc89032d0b4d8a6e6414cd0bfb52b9b49e5ed',9,1,'authToken','[]',0,'2020-09-20 10:44:57','2020-09-20 10:44:57','2021-09-20 10:44:57'),('690b4929520023af8caf443c00ee0ce0367930d048642c49e049c74cc8751a7499d98f38910f19fb',1,1,'authToken','[]',0,'2020-09-18 13:30:15','2020-09-18 13:30:15','2021-09-18 13:30:15'),('6942597233095706dda0125831921a9cdb6db73d734ab01e65d590ad3eec94b58c6f259756c9c44b',1,1,'authToken','[]',1,'2020-09-16 17:31:49','2020-09-16 17:31:49','2021-09-16 17:31:49'),('695266316ce1e7219cca2e262877d27d07983d2f77b88049b739b45716355e95fa9a9b51430edcc7',1,1,'authToken','[]',1,'2020-09-16 17:12:06','2020-09-16 17:12:06','2021-09-16 17:12:06'),('69b8c42dd4dd148f4e85a7a9e95aba506b3d097c26d903546f941ee446368c1706495bbf32a8d54e',1,1,'authToken','[]',1,'2020-09-16 13:39:03','2020-09-16 13:39:03','2021-09-16 13:39:03'),('6a3e26630c983da5bb5caf80de8fc067ad35ed5d7140db76d17b2120521a9d5d7727d17412660f28',1,1,'authToken','[]',1,'2020-09-17 01:43:37','2020-09-17 01:43:37','2021-09-17 01:43:37'),('6a51abc1dc383579c7573cbb16543cf70eb71a82b12adcea4faf275578d9c86aeffad69a8e0655c0',1,1,'authToken','[]',0,'2020-09-18 04:29:05','2020-09-18 04:29:05','2021-09-18 04:29:05'),('6adebece822b798f4313eee3c75c324e04c518665456ef4cce5e7f896b531bccb455c971ef1b69c0',1,1,'authToken','[]',1,'2020-09-17 02:00:37','2020-09-17 02:00:37','2021-09-17 02:00:37'),('6b898f12538258242afabf1443381bd02b8b3b1341a1e787c9320567bf5995c451fb893cc5157acb',1,1,'authToken','[]',1,'2020-09-16 17:28:57','2020-09-16 17:28:57','2021-09-16 17:28:57'),('6b8b70d54cb449d80e6af0c2653debdf5cd78082bd5453d8c80e73b179500d7525663a0d8c745bda',1,1,'authToken','[]',0,'2020-09-17 11:34:38','2020-09-17 11:34:38','2021-09-17 11:34:38'),('6cb24705687250f879df76b26572a562da6e59a8d2a5289177a16080a6a7014db8303af020a504fc',1,1,'authToken','[]',1,'2020-09-16 17:17:33','2020-09-16 17:17:33','2021-09-16 17:17:33'),('6d98c5d4b26d5f1776bffc4e4c6dd1981e864fe63c39ac1e06bf4d2baef2aee93ded357ea914ac84',1,1,'authToken','[]',1,'2020-09-16 16:43:55','2020-09-16 16:43:55','2021-09-16 16:43:55'),('6e3550924435f7512b2d3a8667c27d40a81200c3c9f428d162c7024527cbd709e2a122e44e085c7e',1,1,'authToken','[]',1,'2020-09-16 17:12:07','2020-09-16 17:12:07','2021-09-16 17:12:07'),('6e7630efbbf099db3d80aafa297caac35144ad870b6293cda574eaf527206a55ed03c083ca653d4e',1,1,'authToken','[]',0,'2020-09-17 11:18:27','2020-09-17 11:18:27','2021-09-17 11:18:27'),('6eb2d9204ce6094dbb2381175acb7846f4d0cf799d8c3e4458ee341f4777ac2344391c9f26a2cff5',1,1,'authToken','[]',1,'2020-09-16 13:34:54','2020-09-16 13:34:54','2021-09-16 13:34:54'),('6eb77ab5db1a8cfabb0a4d36bfab58dd2f5dc9f8a88427e74a4abed44c09c0cfd232ebc9011488f4',1,1,'authToken','[]',1,'2020-09-16 13:39:05','2020-09-16 13:39:05','2021-09-16 13:39:05'),('6f0573455a19446cd3a48cfe3c6d39b5799b101f4ca5d8640cde09443ccfda6cc7b690e6955d4ca3',1,1,'authToken','[]',1,'2020-09-16 17:12:14','2020-09-16 17:12:14','2021-09-16 17:12:14'),('6f3971d812cc10bb519cf2fbf9c906bbe577c65d7fecb4c5239c84ab489ac14426cadb45a7800646',1,1,'authToken','[]',0,'2020-09-20 07:41:16','2020-09-20 07:41:16','2021-09-20 07:41:16'),('6f55855dfaaa8693617f19aaac71788175b9b681fdeab5d7d9e65bfb4b977bfe0bef54eebc7a418d',9,1,'authToken','[]',0,'2020-09-20 10:57:58','2020-09-20 10:57:58','2021-09-20 10:57:58'),('6fa93015d2868fe6786b36f8efd1fdc7feb599b2366278c1066e3b1affb63bc50b4c549aae515fa7',1,1,'authToken','[]',0,'2020-09-20 07:34:18','2020-09-20 07:34:18','2021-09-20 07:34:18'),('70987a61301580a5867ddbb83a7e4b04b3418e4c0bcc0ecbddfc738b9067f81f7f63708ca98b2dbb',1,1,'authToken','[]',1,'2020-09-16 17:13:07','2020-09-16 17:13:07','2021-09-16 17:13:07'),('732172b2d6e556616c4fbf31c6b3f03866906622632eace7706774520b714ec08cec11a91e5cd464',1,1,'authToken','[]',0,'2020-09-20 08:10:16','2020-09-20 08:10:16','2021-09-20 08:10:16'),('735d46005113e1c68d1f56d854236027422154753db35d7a341a0b0df7a9c306447480b1bae1c6f5',1,1,'authToken','[]',1,'2020-09-17 01:47:04','2020-09-17 01:47:04','2021-09-17 01:47:04'),('73d6a61310bddcf161b90c44c750ec059a3378054878fca1ef14b12250a8698690ecd21237aca41a',1,1,'authToken','[]',1,'2020-09-16 15:02:58','2020-09-16 15:02:58','2021-09-16 15:02:58'),('7400cd513dd191e951f2e69e8d528989b550d3d50169ea5f646d8d615e28c1ef22c06b850c2f247c',1,1,'authToken','[]',0,'2020-09-18 12:21:04','2020-09-18 12:21:04','2021-09-18 12:21:04'),('75a0016248d9152b9c45b8fc880fba2c7d6c986ff5830984cc53415e69341f9af7e6ebc70ba3856f',1,1,'authToken','[]',0,'2020-09-20 07:31:33','2020-09-20 07:31:33','2021-09-20 07:31:33'),('768da9452ac78f8822c0cc2bfee58f1720dd59c407230db1bb6037a3eb271e5d8ed545e3d2098748',1,1,'authToken','[]',0,'2020-09-20 06:46:33','2020-09-20 06:46:33','2021-09-20 06:46:33'),('76e404c216e79e96b4d1355c1a87c37aa1abb53b5594f0c9a687bbbbd06e0e69d482d6cb37a87eed',1,1,'authToken','[]',1,'2020-09-16 17:23:46','2020-09-16 17:23:46','2021-09-16 17:23:46'),('792a2ca1dd83cb7834cc2317d5d25b294de48c89dc95fdec0804d2f8f1271f196aac4eaf4316aa4e',9,1,'authToken','[]',0,'2020-09-20 10:50:50','2020-09-20 10:50:50','2021-09-20 10:50:50'),('7982764a735d3c40d0fc08122ffa71df25593781ad670e0216ae72bcd903f1b2b56e900daca25873',1,1,'authToken','[]',1,'2020-09-16 13:46:06','2020-09-16 13:46:06','2021-09-16 13:46:06'),('79d9c48b57358f43120df9fb31c2398f2d77bccd2fe3f5a2d9e4b02b66ed5bfcdb7dce7e479aa79b',1,1,'authToken','[]',0,'2020-09-20 07:17:49','2020-09-20 07:17:49','2021-09-20 07:17:49'),('7a0033fd16c58516f8dcb1ff6e0d4cdb123e0d20d124fb402322ebb308e2e80aa3c8cb0718de1b06',1,1,'authToken','[]',0,'2020-09-18 13:34:48','2020-09-18 13:34:48','2021-09-18 13:34:48'),('7a640a4bbf79101c1645dfcba14dc9ce5ed520495882110ef743129699c1ef4b5767497816dce9f2',1,1,'authToken','[]',0,'2020-09-18 01:00:29','2020-09-18 01:00:29','2021-09-18 01:00:29'),('7c7756c78fd5b341a7d059b2caffe8d8edd7612df6892d05295b7e153b5900ec0fb8635e83651c60',9,1,'authToken','[]',0,'2020-09-20 10:58:49','2020-09-20 10:58:49','2021-09-20 10:58:49'),('7d051771d37745066e4f09073a9948560b1595e9e34d05d89d4172498171f98f4ade3bf16f2f9524',1,1,'authToken','[]',1,'2020-09-16 13:35:29','2020-09-16 13:35:29','2021-09-16 13:35:29'),('7da82871233f35e198248bef222c2428676d1798985ae99c4f8ec23a9e3ec7c03d61772a0005dcad',1,1,'authToken','[]',1,'2020-09-16 17:12:18','2020-09-16 17:12:18','2021-09-16 17:12:18'),('7ddf5e171074090fe97548a12261f5ebad9bf7a64bd4974d7ad4dbf7fe59c966eaff8d025f739caa',1,1,'authToken','[]',0,'2020-09-17 11:19:31','2020-09-17 11:19:31','2021-09-17 11:19:31'),('7df89f3a6f848cf78ce2e8ba7c972f6fd39ba9396d42f49b22c65f9970853fea70c6f118d0c31a22',1,1,'authToken','[]',0,'2020-09-20 07:30:03','2020-09-20 07:30:03','2021-09-20 07:30:03'),('7ed7ee5ce5a7b9e93f899be6601191450ada8b0d5d2020374739f774642fc640dfa9468e4f148fdb',1,1,'authToken','[]',0,'2020-09-20 07:16:56','2020-09-20 07:16:56','2021-09-20 07:16:56'),('7f05f2434e349734d4c3d0b2dadb3f131dcfffc599b480a3990b4ea0fe02c87c9ead26bb0beaf145',9,1,'authToken','[]',0,'2020-09-20 10:42:22','2020-09-20 10:42:22','2021-09-20 10:42:22'),('7f6b0b7d264916ff371a1042d8a6d96a6d3c8e8b40b4e6f7af1244235045669f8ef447313c622395',1,1,'authToken','[]',1,'2020-09-16 13:50:34','2020-09-16 13:50:34','2021-09-16 13:50:34'),('7fc7cd63d526a6f232d420e7b37aebc08917c4def994bea74bb6c1fd8b3e45779bb32290780bd2ae',1,1,'authToken','[]',0,'2020-09-20 08:23:55','2020-09-20 08:23:55','2021-09-20 08:23:55'),('7fd6ca521c628206c5be8e23bc477c9e88ef3ead9514f790920e3faa5c48d24323868bfaf235af82',1,1,'authToken','[]',1,'2020-09-16 15:58:02','2020-09-16 15:58:02','2021-09-16 15:58:02'),('7ff1a0abfe1df2631c28325a6e3258d5797f3f3999777de6c954383e0f328683d1b7ec394647176c',1,1,'authToken','[]',0,'2020-09-18 01:01:51','2020-09-18 01:01:51','2021-09-18 01:01:51'),('80097cb28b97ee3be72e35f5173fcc5b46127eec3487f6f627aae4ed3b6580dc637ebaeb715dd903',1,1,'authToken','[]',1,'2020-09-16 17:17:04','2020-09-16 17:17:04','2021-09-16 17:17:04'),('80e4b17dc9d1fd7dc483a7b9a119abc4468ae4587942b8d68dc8f9c012e9798e76c4ef975e5dc848',1,1,'authToken','[]',1,'2020-09-17 01:43:20','2020-09-17 01:43:20','2021-09-17 01:43:20'),('810418e301c4425b868b0c6bbbb450bd70b3e8d335896a2e5019505f59db1a034214736362a064ea',1,1,'authToken','[]',0,'2020-09-18 13:35:27','2020-09-18 13:35:27','2021-09-18 13:35:27'),('81c82e1ecec7f50f7f3064ad6b71bbf6fc5f22995de2ad39298508eda9ab29269de6532a0dca8f96',1,1,'authToken','[]',1,'2020-09-16 17:23:50','2020-09-16 17:23:50','2021-09-16 17:23:50'),('81f997d8c6e5bd6469a3514c9da32af7ce94c5690c162e465d34358196b6926988e456b6d3192a4c',1,1,'authToken','[]',0,'2020-09-20 07:09:24','2020-09-20 07:09:24','2021-09-20 07:09:24'),('8291cc5276f02f2507fa465b2aff171c0a830550a0f70660ccb05f76c55ae43c89367bf90b32ceeb',1,1,'authToken','[]',1,'2020-09-16 17:23:10','2020-09-16 17:23:10','2021-09-16 17:23:10'),('829bf67e2851106990601242ec776d2347d3580318ce4e1d055344ffc57d333c30369c8f217000c2',1,1,'authToken','[]',0,'2020-09-17 11:37:31','2020-09-17 11:37:31','2021-09-17 11:37:31'),('82e6c2ec2e41ebced37fc1c7bd0e6ac61b25f889e2197cdfe54f2be652d606c0a610b989a615d0e4',1,1,'authToken','[]',0,'2020-09-20 09:43:02','2020-09-20 09:43:02','2021-09-20 09:43:02'),('8346e537117272671fe396ff4542160d050b0006cf573d6ddf1e636f1adf58c4f044506b8a0ce772',1,1,'authToken','[]',1,'2020-09-16 13:48:59','2020-09-16 13:48:59','2021-09-16 13:48:59'),('83ff1592104793a6952664d210a5835af391473b8c51bf7b5d04c7594b8b321ada9c69d8f3ab12e8',1,1,'authToken','[]',0,'2020-09-20 07:16:24','2020-09-20 07:16:24','2021-09-20 07:16:24'),('8482928a8d581900dd5b6105fe7f39a3ea61db0de6105240cc8733d794f2fe2e3ae5b84793e4b749',1,1,'authToken','[]',0,'2020-09-20 08:33:05','2020-09-20 08:33:05','2021-09-20 08:33:05'),('865f8318bc5b4fc276e7e3874aeef681be66cb0d3a4278052be7f557a0798261c1a433d4063f80ee',1,1,'authToken','[]',0,'2020-09-20 07:12:28','2020-09-20 07:12:28','2021-09-20 07:12:28'),('868434af41f741b104882135a0eab52945f2027c5ddd6654a349bcc82d0ad7a6cb61c03f1317a3dc',9,1,'authToken','[]',0,'2020-09-20 11:12:08','2020-09-20 11:12:08','2021-09-20 11:12:08'),('86bde103eda3ec7d7f3f55d434cac1354b526128f8d51dcffd968d2e20565ac75339c5fd86e05635',1,1,'authToken','[]',0,'2020-09-20 07:16:46','2020-09-20 07:16:46','2021-09-20 07:16:46'),('8710318d3fc409876980623ed6101cbc237b67440cffe9594e467d1097c1caf3ac4f40a5e131ae52',1,1,'authToken','[]',1,'2020-09-16 13:57:05','2020-09-16 13:57:05','2021-09-16 13:57:05'),('8735f1ffc099054fed8f9a92e2c138d1c09426a28f19296d9d6e51d9d3670b5acc0b289ffbdbba1d',1,1,'authToken','[]',1,'2020-09-16 17:12:04','2020-09-16 17:12:04','2021-09-16 17:12:04'),('874c8324feac493077b5fbebdc7b257ed84c73f5e299e7e467bc8b26df71d79a84f9628295e00a8c',1,1,'authToken','[]',1,'2020-09-16 16:15:51','2020-09-16 16:15:51','2021-09-16 16:15:51'),('876639375984876ec6c08eac382ad2161d1c3b362ab4b8900ae50a765c3a5059a63f784537059228',1,1,'authToken','[]',1,'2020-09-16 17:12:01','2020-09-16 17:12:01','2021-09-16 17:12:01'),('885d785d5b2f6bd4561f1d47def1e095ba11072219367003604c167d1caf180de4a62253cc04b5ac',1,1,'authToken','[]',0,'2020-09-20 07:30:03','2020-09-20 07:30:03','2021-09-20 07:30:03'),('88a55af10e93b6bfbb76bccf0384f457791e3698a2069ee0771586c69f618604c9f9849f000d3e84',1,1,'authToken','[]',1,'2020-09-16 13:48:52','2020-09-16 13:48:52','2021-09-16 13:48:52'),('88feb7a0d6729704c6daf82bc778891c35e8d8172b9b976eb69a7776bf839873e2b244187bd9a4e7',1,1,'authToken','[]',1,'2020-09-16 12:10:39','2020-09-16 12:10:39','2021-09-16 12:10:39'),('890862cc1f66473045ceaf0cd5c88dd9f0f39f43e35576a54802bdd60d69f65418b5608672969420',1,1,'authToken','[]',0,'2020-09-18 12:10:33','2020-09-18 12:10:33','2021-09-18 12:10:33'),('8a81be0f009d981caff8c495987d0de6d51a791e02dad8a4d48e877f726fa4a1507814f5365f2078',1,1,'authToken','[]',1,'2020-09-16 16:37:13','2020-09-16 16:37:13','2021-09-16 16:37:13'),('8b3aaa169b8602c23055ec79f2d2e06aaea5d3bb7e88c0cfc28db2a14666e7a6e1fac8c74d00732b',1,1,'authToken','[]',1,'2020-09-17 01:43:11','2020-09-17 01:43:11','2021-09-17 01:43:11'),('8b790a1435b7c774297045d1328bb855eb008719dbc7824d0d243f3e6b57bf61e9a096d1838d6ab6',9,1,'authToken','[]',0,'2020-09-20 10:51:52','2020-09-20 10:51:52','2021-09-20 10:51:52'),('8b88c46848e714161146af74156ff47b3a7a4e6f65712cd8387bb7e017a063f68a226b142a85ae71',1,1,'authToken','[]',0,'2020-09-20 08:11:21','2020-09-20 08:11:21','2021-09-20 08:11:21'),('8c49de7c5ec55d6ad3b54a2784187e37120892aba1fe3e5aef0f653187221e984ab6f892838f5479',1,1,'authToken','[]',0,'2020-09-20 08:31:56','2020-09-20 08:31:56','2021-09-20 08:31:56'),('8c5961b055665e24a683c4cfa92983c61a2d3f8f812e056afab5ad6dd576888541caa4eee99d4f1b',1,1,'authToken','[]',1,'2020-09-16 13:46:06','2020-09-16 13:46:06','2021-09-16 13:46:06'),('8c78a3ef6d486fe1e965240403ffbf910416e1be250e7442a3d8208e016b23ef42481cac41c9cff4',1,1,'authToken','[]',0,'2020-09-20 08:09:34','2020-09-20 08:09:34','2021-09-20 08:09:34'),('8c87e087f46e4f11339355e2bc76aeaf500a97712ef0ae756157cbb7918ca930e4bd99d4ea0f9999',1,1,'authToken','[]',0,'2020-09-20 07:13:10','2020-09-20 07:13:10','2021-09-20 07:13:10'),('8d43e4ce943b08683f4eed66de74f4c5977e50fcd50b65850b63edbdd5cb13de45f7a9cc2d8b3e8d',1,1,'authToken','[]',0,'2020-09-18 13:26:42','2020-09-18 13:26:42','2021-09-18 13:26:42'),('8da3cf5308f9ce1dcc4b106b91ce034d3fe9969bfb87db30dec0824ad92fa4ba2c508268e3881415',1,1,'authToken','[]',0,'2020-09-20 07:31:43','2020-09-20 07:31:43','2021-09-20 07:31:43'),('8e4ec67ed23cb0fa8a65a7b7fe207892ed804d74eed182d28e80c8fcca950046b8927ecc490fc2e1',1,1,'authToken','[]',0,'2020-09-18 12:24:00','2020-09-18 12:24:00','2021-09-18 12:24:00'),('8e6b1112f533a97198eeac5e7cae161234c81f828237a8e3794c4e4677d32087835106e5fceb0650',1,1,'authToken','[]',0,'2020-09-20 07:07:27','2020-09-20 07:07:27','2021-09-20 07:07:27'),('8f283571188bbdd613ceb3a611b31b084adfd47c6e31a9a214731dd67bdaf33cd268be3251be9f2f',1,1,'authToken','[]',0,'2020-09-20 07:41:16','2020-09-20 07:41:16','2021-09-20 07:41:16'),('90e03866c3b10351163a47d028b3bdde504d7ebd608506123ed293a3234a1051ae31dade8b68564c',1,1,'authToken','[]',0,'2020-09-18 12:07:13','2020-09-18 12:07:13','2021-09-18 12:07:13'),('91a0ee951b55be60e7efeadb1183657bd89b38734b3f74e277a2b67bd67ef9177616027e5c22954f',1,1,'authToken','[]',0,'2020-09-20 08:13:13','2020-09-20 08:13:13','2021-09-20 08:13:13'),('9234d06978842dca1feececfb8b8afa3781cf35178edfdb26c42c1429a79148627784a0fb8e1b641',1,1,'authToken','[]',1,'2020-09-16 17:19:13','2020-09-16 17:19:13','2021-09-16 17:19:13'),('928cf065a99092b8b8260e132b1e0fa3611792fdd7b673bdbc7c4019d9083ddb0091afd3ab69e9c2',1,1,'authToken','[]',1,'2020-09-16 15:58:04','2020-09-16 15:58:04','2021-09-16 15:58:04'),('9296a8be2209889148ed64573302f8a0a5d612b9f2d516152f4f582ebc09e81cb45459e2db52da2c',1,1,'authToken','[]',0,'2020-09-20 07:13:11','2020-09-20 07:13:11','2021-09-20 07:13:11'),('938cb0ec5b76c7d0e285b96c363cb025a8ebba4f6db2f532c1ffca26f7e4961b6a18384646470751',1,1,'authToken','[]',0,'2020-09-18 01:01:49','2020-09-18 01:01:49','2021-09-18 01:01:49'),('93da7718ffcd0fe7f946fd96d3aab5cf6e0ddc313e1ed54f8f2fe85808492580d85eeeb54a6d0704',1,1,'authToken','[]',1,'2020-09-17 01:59:31','2020-09-17 01:59:31','2021-09-17 01:59:31'),('93f97fa68cafb91cb36eeab966c56c30b6cd5ed316a177f3216cc1ae9d237b5a174c3d681e5cf887',1,1,'authToken','[]',0,'2020-09-20 07:29:25','2020-09-20 07:29:25','2021-09-20 07:29:25'),('940272e6b2414a4cc18aa1567873641071d1a653d6e071d49202c383a4d7e96c35273a572172699f',1,1,'authToken','[]',0,'2020-09-17 12:36:21','2020-09-17 12:36:21','2021-09-17 12:36:21'),('9427d57f4bd52cd9c8101f3c85e04fac609970a499159ca45acf7c01a921a6c0c737090282b56104',1,1,'authToken','[]',0,'2020-09-20 07:07:12','2020-09-20 07:07:12','2021-09-20 07:07:12'),('94669dfa60cbb5c0d8fe4916378f76dfc86f8d025e558ec68e7b24b34edecddf71054e04c71f0ba5',1,1,'authToken','[]',1,'2020-09-16 14:47:03','2020-09-16 14:47:03','2021-09-16 14:47:03'),('947d7e75289fef159356d9575080cdc87bca28f940024ffd0664a16ef4543c70e51d0268a7e8bb7a',1,1,'authToken','[]',0,'2020-09-20 07:16:22','2020-09-20 07:16:22','2021-09-20 07:16:22'),('9519b8cf5de1eef1d38f200aba6fd46ac18ccccf1a37d4e7a68af5205338973bd17d3b15f232f692',1,1,'authToken','[]',1,'2020-09-16 17:24:02','2020-09-16 17:24:02','2021-09-16 17:24:02'),('954717e23fc0bf993ad643c1ac8aad28bcbbdc3ad3847893da12bc3e0dcc901237793b330a78aaf5',1,1,'authToken','[]',0,'2020-09-20 07:24:53','2020-09-20 07:24:53','2021-09-20 07:24:53'),('9557dfc68e86a6bf2b10f75b2a0acf7c9ade98738cc906ca2bf2a31818ad6466d70a70cbae759d7c',1,1,'authToken','[]',0,'2020-09-20 07:16:31','2020-09-20 07:16:31','2021-09-20 07:16:31'),('960a4d1d56a616b36856e147db652faca3bc7fd4d7e2f1063a5927108c7a9952c908163bea7262b8',1,1,'authToken','[]',0,'2020-09-18 13:42:59','2020-09-18 13:42:59','2021-09-18 13:42:59'),('9631ba69647a7970b5aa5aa846753a26f087a906d05e46b7ec47f3e94c781b19543693e460e30479',1,1,'authToken','[]',0,'2020-09-17 11:19:01','2020-09-17 11:19:01','2021-09-17 11:19:01'),('967287983166b4d4bb81ec7d0610ed9c4b0b75c012777d5025f9949452af0c027e92eb8dc01f593d',1,1,'authToken','[]',1,'2020-09-16 17:28:36','2020-09-16 17:28:36','2021-09-16 17:28:36'),('967ceb7058029270151a3d94f92bec6d41cd2abfecc1151fb8ea0dd54b89cf5354593eaf7f47427c',1,1,'authToken','[]',1,'2020-09-16 17:12:08','2020-09-16 17:12:08','2021-09-16 17:12:08'),('96c8064b54edfb27d0a2a2e817ef0b166f9f44afd6205f6f5e84596d9f94901c4ae4dcb97db055a7',1,1,'authToken','[]',1,'2020-09-17 09:47:55','2020-09-17 09:47:55','2021-09-17 09:47:55'),('97ddb4314b7d8ee329741e702ffa25160aea5f33effb3b311f4543876d8693f24c6630d5dc20c7b9',1,1,'authToken','[]',0,'2020-09-20 07:04:07','2020-09-20 07:04:07','2021-09-20 07:04:07'),('981fff0104c48afb4464a2e2e46477d427d374197c47cd8c98089354a6921e2246b57cabc73b4f29',1,1,'authToken','[]',0,'2020-09-20 08:23:49','2020-09-20 08:23:49','2021-09-20 08:23:49'),('982d21eb10a44f459d88d30fc28595f6019a83e467229e941de1fd50cb8aef0d802c707b42c108e2',1,1,'authToken','[]',0,'2020-09-18 12:18:24','2020-09-18 12:18:24','2021-09-18 12:18:24'),('9867ed3b6e09299ae82f8c1d4453761184a551a1ddb6d9aa067d8fd05bedf806242751a31e95a441',1,1,'authToken','[]',1,'2020-09-16 17:13:27','2020-09-16 17:13:27','2021-09-16 17:13:27'),('9933b0a0a696043c97a53a351821a0fe38406559b610bbad4ab83ad8ce160506c62fb201617277b6',1,1,'authToken','[]',1,'2020-09-16 17:16:32','2020-09-16 17:16:32','2021-09-16 17:16:32'),('99c9404defe854bf5a8ecef65eec438803e65ef8e50b7999f514c6a36a9b2135c159950c4658e307',1,1,'authToken','[]',1,'2020-09-16 13:54:14','2020-09-16 13:54:14','2021-09-16 13:54:14'),('9abf490800aeab208f2b4f64028aa468effa63f8a51841d47e91b792d8fc58eeb7e487ce31ba454f',1,1,'authToken','[]',1,'2020-09-16 16:37:13','2020-09-16 16:37:13','2021-09-16 16:37:13'),('9be51892be025ec5f4c79a679fe975048ee3dcbda254da2c583cb6306d768476c4880996a2976d76',1,1,'authToken','[]',1,'2020-09-16 17:27:12','2020-09-16 17:27:12','2021-09-16 17:27:12'),('9c0200ce7db698d54cde356b0a6c328e3c7a82acceaed5597c2feface5efddab9cd32598bf973a1e',1,1,'authToken','[]',1,'2020-09-17 01:51:12','2020-09-17 01:51:12','2021-09-17 01:51:12'),('9c09b88a20a74ed045385c2998051bcea08a70297b4219eb6549d3160720c2dd0254b8783d4e30f4',1,1,'authToken','[]',1,'2020-09-16 13:49:13','2020-09-16 13:49:13','2021-09-16 13:49:13'),('9c1e7c5d3be0059a57ffb809be79bf7bf73fbc08744b25964cebbd43d15f59b5ce2801ef88742594',1,1,'authToken','[]',0,'2020-09-20 07:29:25','2020-09-20 07:29:25','2021-09-20 07:29:25'),('9c3ae3d42197d316ed2085d00b9448be4d47a0b40ae94e95ac541578173bf0484a16dd50c494a597',1,1,'authToken','[]',1,'2020-09-17 11:05:13','2020-09-17 11:05:13','2021-09-17 11:05:13'),('9ca786966ca662178e3aef32725e01f2dbc6b698240bb1bbb44a6fac9409ee09e403d6069a635972',1,1,'authToken','[]',1,'2020-09-16 16:49:05','2020-09-16 16:49:05','2021-09-16 16:49:05'),('9d2baf8d12d32623ebb199e61ffee7f452dd3b4a4353643ea367f9776169fe4f78a835627e03c811',1,1,'authToken','[]',1,'2020-09-16 17:32:19','2020-09-16 17:32:19','2021-09-16 17:32:19'),('9de613717d92c03a646ec4f6ad3b8c0a78918d4f8bc508287912a5ca9e33ce55585543e48cc63d3c',1,1,'authToken','[]',0,'2020-09-18 12:27:55','2020-09-18 12:27:55','2021-09-18 12:27:55'),('9f01c54417e265bda396fcce5d1f6276ee192a30ab6adbb782e929b17aca3d298b2cfb9cfc37c3a3',1,1,'authToken','[]',1,'2020-09-16 13:54:29','2020-09-16 13:54:29','2021-09-16 13:54:29'),('9f6d315bcee15dacb1447c969238ee7976ce5a5d5e1fe54dc2be05de8852f22ad0376bf572668c95',9,1,'authToken','[]',0,'2020-09-20 10:32:19','2020-09-20 10:32:19','2021-09-20 10:32:19'),('9fbbede8d59aaa92e1ef6eebec2ae89724216edf99bce80e655dec9f35a1b0dd467ac9533e3cf62c',1,1,'authToken','[]',0,'2020-09-18 13:24:49','2020-09-18 13:24:49','2021-09-18 13:24:49'),('a081c2affb230b66860c6a5fa123caaf3d84d06446b184938bac13734346d4c73580d02bb48da7e9',1,1,'authToken','[]',1,'2020-09-16 17:28:58','2020-09-16 17:28:58','2021-09-16 17:28:58'),('a10b89151a01e8c1c70f714205a0c2996fdd75b9ccc746f2e01a00f89acd7b7507d72386f9ac5df3',1,1,'authToken','[]',0,'2020-09-20 07:15:07','2020-09-20 07:15:07','2021-09-20 07:15:07'),('a147e9c9ab8f0153b79a4d14e74f95aed45e0af46fe8a0139c9d4d8ef357f31854406d630c691baf',1,1,'authToken','[]',0,'2020-09-18 12:19:03','2020-09-18 12:19:03','2021-09-18 12:19:03'),('a19a567536714cb024709bf50b80b959f040da56d8c8d83ccbca8eec687ecd12051b4d3bfae3d797',1,1,'authToken','[]',0,'2020-09-18 01:01:53','2020-09-18 01:01:53','2021-09-18 01:01:53'),('a1aba7366fa8b32c287a71d7706f9dfc5217711fca3f1b16ffee5b86131fb6d2ce104eea0af004a6',1,1,'authToken','[]',0,'2020-09-18 13:43:31','2020-09-18 13:43:31','2021-09-18 13:43:31'),('a1b559d1a6d95b090908c479becf3fda9c71d49b5ef36b5c44a28731a3848ab49796e63bb00c4b96',1,1,'authToken','[]',0,'2020-09-20 07:25:47','2020-09-20 07:25:47','2021-09-20 07:25:47'),('a26301e9f9d7993bfa39c0487c9e8c4cfe426bef41da6ed2175aacdb3b07af8013b0b0bd3517d4d7',1,1,'authToken','[]',0,'2020-09-20 07:12:54','2020-09-20 07:12:54','2021-09-20 07:12:54'),('a2911f8028ee670ff7874361e814948d4ad46b79f52e41244847020d24e4d4936ea5fe0e288493ab',1,1,'authToken','[]',1,'2020-09-16 13:56:03','2020-09-16 13:56:03','2021-09-16 13:56:03'),('a316c8e2d8ba194f2c979640d9ce06b806219520adc5c246b16ef713fdf49aec8ccae6d6146ced97',1,1,'authToken','[]',1,'2020-09-16 17:28:58','2020-09-16 17:28:58','2021-09-16 17:28:58'),('a3695a32a79e6569271c478618f374a38c5abe77cee8a50170f1c4a8e3d1dfc4dead7fc39dcf3839',1,1,'authToken','[]',1,'2020-09-16 13:40:28','2020-09-16 13:40:28','2021-09-16 13:40:28'),('a45d4114ec8984b9e8c8e73c63bca122fdf272ff969ba918c2b550a408d40549ba0be44125f427f8',1,1,'authToken','[]',0,'2020-09-20 07:13:53','2020-09-20 07:13:53','2021-09-20 07:13:53'),('a4913537f9857c5108e1af1ded98632ffcc00a42dd4ca91e45026f8a6346c97e5ac6c0a726d97809',1,1,'authToken','[]',0,'2020-09-20 07:31:33','2020-09-20 07:31:33','2021-09-20 07:31:33'),('a4f9a0f8796f327722cb75fd1009c20c9eeea072945d35f62fc6fed88729f76d9298f82c7c583c25',1,1,'authToken','[]',1,'2020-09-17 01:43:08','2020-09-17 01:43:08','2021-09-17 01:43:08'),('a53735b006cd8c3c9e8bad3ea7fc488ccbf75c81d276d475e8dec6bb93e16742273f60a34d186757',1,1,'authToken','[]',1,'2020-09-16 17:12:03','2020-09-16 17:12:03','2021-09-16 17:12:03'),('a5550e388eca6edc8178eed98ee943cf9c3cb733c5f812e5d843c966468c19f2a1acf6ca023ac6e2',1,1,'authToken','[]',1,'2020-09-17 01:59:19','2020-09-17 01:59:19','2021-09-17 01:59:19'),('a5fe6906a8fadd116400292bf483d744083c05a0fd8f39a86d4b21f057140919c0a9d93787820bf3',1,1,'authToken','[]',1,'2020-09-16 13:44:21','2020-09-16 13:44:21','2021-09-16 13:44:21'),('a60e551f3831b7195e2a5cf00c6f330f43ce7f57ab7ffd5f4ab626512049cbab4ddfae4dee27fa76',1,1,'authToken','[]',1,'2020-09-16 13:39:05','2020-09-16 13:39:05','2021-09-16 13:39:05'),('a61032c05e5fae0dced03d895482ef75d9b66b300b194d2abd44c6f4000d628d158155a2aafa7771',1,1,'authToken','[]',1,'2020-09-17 01:45:24','2020-09-17 01:45:24','2021-09-17 01:45:24'),('a746e5ca3b2083c5fec46eac4817700e0a40dca5ec2e08649acb6d18c6ea7b658e1da0147f5b9738',1,1,'authToken','[]',0,'2020-09-18 04:29:00','2020-09-18 04:29:00','2021-09-18 04:29:00'),('a76bf6a144cacd9d51ad11f64ee9c19f5fe14ab8e0ab7aff76a70ac5abd7195e54115a9905175c27',1,1,'authToken','[]',0,'2020-09-18 12:07:13','2020-09-18 12:07:13','2021-09-18 12:07:13'),('a8c9306d2e48c67531a334cc9ac274099904411228f0f142d7ab35f427bcccd717d4cdb879578671',9,1,'authToken','[]',0,'2020-09-20 11:19:14','2020-09-20 11:19:14','2021-09-20 11:19:14'),('a98a5524171052ed20fb09e310f58ed3644acaf2c33f92875df61e0ac3341fd59da313838fa3564a',1,1,'authToken','[]',0,'2020-09-20 07:20:47','2020-09-20 07:20:47','2021-09-20 07:20:47'),('a99d36c2d220e70a226d1128ed06e59e2652af1a8c319428a95e5de59e8bdff27d7639cea5e499e2',9,1,'authToken','[]',0,'2020-09-20 11:19:14','2020-09-20 11:19:14','2021-09-20 11:19:14'),('ab1d786bd62d6c312e546cc1820e7315d63692ef379c0f0a343951e3d407238ec1cc5da6413972a4',1,1,'authToken','[]',0,'2020-09-18 12:16:30','2020-09-18 12:16:30','2021-09-18 12:16:30'),('ab372963aca797ab69d35c842d2912572a9d08522cfba44270538406055dc1231c8db4e74da4d1ee',1,1,'authToken','[]',0,'2020-09-20 08:10:09','2020-09-20 08:10:09','2021-09-20 08:10:09'),('ab9074cfb4075b65eaabe742cbee7cac4c316c612b4fe7f27728c4573a8959edf3c40c13a1b70cc7',1,1,'authToken','[]',0,'2020-09-20 06:56:58','2020-09-20 06:56:58','2021-09-20 06:56:58'),('ac7b1e7c2dcdb99ef187a8327442b394911b4a21a2d792a5daff83add10c7fcaab212c300b0c7bb2',1,1,'authToken','[]',1,'2020-09-16 13:37:59','2020-09-16 13:37:59','2021-09-16 13:37:59'),('adb153da740bd123866bbf22a588abe66e87e715344953cefbf7fcd029eedb8433a4f58d4f42471e',1,1,'authToken','[]',1,'2020-09-16 15:58:06','2020-09-16 15:58:06','2021-09-16 15:58:06'),('ae072a3b8f3a9ffd59abc2b887353e9da0b82526808bee525a522f10f3a48b1bf2f8ff55b667141d',1,1,'authToken','[]',1,'2020-09-17 01:59:47','2020-09-17 01:59:47','2021-09-17 01:59:47'),('ae6ee1b4d5fe20611217574a84e60d8da92c45539ec43d8e59ad94b363f0bf24cad2903cf0d261ac',1,1,'authToken','[]',0,'2020-09-20 07:58:28','2020-09-20 07:58:28','2021-09-20 07:58:28'),('aee3359aadbc7fd0f08896a2f478072c550aa39fbca0b358300c7b5a079df67eac6e1053a323997d',9,1,'authToken','[]',0,'2020-09-20 10:46:11','2020-09-20 10:46:11','2021-09-20 10:46:11'),('b06b802e7c34c51e969de772ace736dfad3acc433db1c074b7fd1ae91c782931fec1f1beadddb2dd',1,1,'authToken','[]',0,'2020-09-18 13:25:14','2020-09-18 13:25:14','2021-09-18 13:25:14'),('b084117a9205318644ded24b5f5ba1087253484238714bdd936f43815b440c28eb857a27bb16079a',9,1,'authToken','[]',0,'2020-09-20 10:45:02','2020-09-20 10:45:02','2021-09-20 10:45:02'),('b1a9c68a97e8f15148f1582136450d5fa3b10808bd268623dafee0a75d43d67bc41bb6b6f1b05ad0',1,1,'authToken','[]',1,'2020-09-16 15:58:09','2020-09-16 15:58:09','2021-09-16 15:58:09'),('b2777bbe5717f6b3ddee9f4788044b19c7491a1e592e846008f221c8cf45a15751536a2479f47af9',1,1,'authToken','[]',0,'2020-09-20 08:10:16','2020-09-20 08:10:16','2021-09-20 08:10:16'),('b29c2b4a77edde12efe6950e88dc1e054be4739d8b54d629c5482558273878348a661dea07a878b3',1,1,'authToken','[]',0,'2020-09-18 12:25:46','2020-09-18 12:25:46','2021-09-18 12:25:46'),('b2cf919fc1eb02ac2860c5c334a7ae77a526e9ab0c4ce965f84bce5d7b687eeab0ab41f4df0ac84f',1,1,'authToken','[]',1,'2020-09-16 17:13:03','2020-09-16 17:13:03','2021-09-16 17:13:03'),('b315abfe077b45ca7d2114eace8f89d9ad23e20db4990a673086af3e3d7b0e8aa4a12fde8b16dff1',1,1,'authToken','[]',1,'2020-09-16 13:50:34','2020-09-16 13:50:34','2021-09-16 13:50:34'),('b3190ccc424a475b70ad6671c62a752de6d96e0e0a0476479d91547c8a1517197bd87e26e52448f7',1,1,'authToken','[]',1,'2020-09-16 17:32:37','2020-09-16 17:32:37','2021-09-16 17:32:37'),('b3d4c2672558ad65f26f9a9ffae1c79ec4298b90c4277b2ef75f8d2722a925cef9fbedd1aa523af7',1,1,'authToken','[]',1,'2020-09-16 12:12:06','2020-09-16 12:12:06','2021-09-16 12:12:06'),('b496de3f755bce73b964fb886ea1a7ac62ee2c80eb0a8dfb6a53c734f5ae3e3a99f04f77e0435cfd',1,1,'authToken','[]',0,'2020-09-20 08:39:14','2020-09-20 08:39:14','2021-09-20 08:39:14'),('b4ca943a295c10b6c20be75ca7ca54b90ea4cdcca746854b0e7baf3c4e0769fbc99b68c1e018ed01',1,1,'authToken','[]',0,'2020-09-20 07:12:58','2020-09-20 07:12:58','2021-09-20 07:12:58'),('b4f4284d06b02ae2737c16a9221a9ee3ba52039dc07b4959ed1b21c272ba571fbbc14f692a1e0233',1,1,'authToken','[]',1,'2020-09-17 02:00:23','2020-09-17 02:00:23','2021-09-17 02:00:23'),('b6443f153e563782037d1b5e9e3f0d83a5f70259e6ca41aa17f4b3754ea8739c9459869aeefb45f1',1,1,'authToken','[]',1,'2020-09-16 17:12:15','2020-09-16 17:12:15','2021-09-16 17:12:15'),('b6503dd927ea26c4d9cca3cebba33e04265e47d1bea4a4c9375c8ee7016d5a589c3262cda74fff68',1,1,'authToken','[]',0,'2020-09-18 12:10:37','2020-09-18 12:10:37','2021-09-18 12:10:37'),('b7821e18abf0bc621c6e93fcdda3f0019eb3ad33a9b1c665e8f8c632b9886ad3e27d5cd419bd7f64',1,1,'authToken','[]',0,'2020-09-18 13:30:59','2020-09-18 13:30:59','2021-09-18 13:30:59'),('b812d6b0a83b4dd3d221f5def614a2328002fcb30e04ebf54a0e5fabf85f53f805763a4eb4994420',1,1,'authToken','[]',1,'2020-09-16 17:16:46','2020-09-16 17:16:46','2021-09-16 17:16:46'),('b85843db7ad7795e0f8074a00a95f00da22ee4fb8d6e5f36806897f6a0787d252ae698d7caeb0d75',1,1,'authToken','[]',1,'2020-09-16 13:47:19','2020-09-16 13:47:19','2021-09-16 13:47:19'),('b86b9c5c572a9bd10d143fe708b0d9005147ec31276cc239535354ffe5da845409e6eee45e31b057',1,1,'authToken','[]',1,'2020-09-17 01:55:02','2020-09-17 01:55:02','2021-09-17 01:55:02'),('b957f2beb5bdf2f7ba2bdc4ee731570f021d1c13413070f1eeecd86367346b8fb30dfc50b5339b35',1,1,'authToken','[]',1,'2020-09-16 13:49:13','2020-09-16 13:49:13','2021-09-16 13:49:13'),('bacd386d5e2c09429d0ae65cfa7202a49be244ed42e5d232d158cd0c1381cc7d617c4b2a7ab5e292',1,1,'authToken','[]',1,'2020-09-16 13:35:29','2020-09-16 13:35:29','2021-09-16 13:35:29'),('baf78be83e6ab8e13dc698f65dd261940ec0f14a84196768eddf4bd9d65682c0138e98acbacb9858',1,1,'authToken','[]',0,'2020-09-17 11:19:01','2020-09-17 11:19:01','2021-09-17 11:19:01'),('bb22a75458d5400e1b7fc48fb2ebdfbafd1ec119b891c3e276833945e3e7b0a8b2ad35cf5dd5101a',1,1,'authToken','[]',0,'2020-09-18 12:19:21','2020-09-18 12:19:21','2021-09-18 12:19:21'),('bb59b622b035289f95a2912cf16748eb3ec5906e834884b07141ef1d981d2df2bb4fccaffb577ba1',1,1,'authToken','[]',1,'2020-09-16 17:20:07','2020-09-16 17:20:07','2021-09-16 17:20:07'),('bb59c56b08812f538237170c6551d517632039b2b63c2f7ac6217fc10494cf191f9b9465c7563195',1,1,'authToken','[]',1,'2020-09-16 17:12:09','2020-09-16 17:12:09','2021-09-16 17:12:09'),('bc21381a9228ad6cb5cc1bc0d9d4f3d88341210c01618703739df03c93aa7e298f612f4d711c208a',1,1,'authToken','[]',1,'2020-09-16 17:32:59','2020-09-16 17:32:59','2021-09-16 17:32:59'),('bc32644dc4364b3d6f62bd3abfa34ae0ef5db3841f5b52720c715bf5dac4fbbcab63f052c7ba1083',1,1,'authToken','[]',0,'2020-09-20 07:13:07','2020-09-20 07:13:07','2021-09-20 07:13:07'),('bdbf54c7aded9485d3f2d87e8912b60d3e9cf5f93b76ad3b0b24a182a61afb34011bab2a38b52b37',1,1,'authToken','[]',1,'2020-09-16 17:12:14','2020-09-16 17:12:14','2021-09-16 17:12:14'),('bfb4cc888becfa5bf11388bd9d427eb9d390d5076c196a0eaeab80316bccc7623d54e9193a5a9058',1,1,'authToken','[]',1,'2020-09-16 13:48:34','2020-09-16 13:48:34','2021-09-16 13:48:34'),('c094d5630f9fd7e3e3a0090511a8349f95ec503b0827028e58d68ba5f84ed5d5050a5ae15cbe113c',1,1,'authToken','[]',0,'2020-09-20 07:11:58','2020-09-20 07:11:58','2021-09-20 07:11:58'),('c14c077e599db45dee7d7c888eaed4e930e9306967d1335ccf3afcd0fa9a787e600a9bda9d4d4b29',1,1,'authToken','[]',0,'2020-09-20 08:12:57','2020-09-20 08:12:57','2021-09-20 08:12:57'),('c213e69dfb8cd607dcbd99c7624dee1c87f6305868ccc377f21342ea9f2568410c43f7330c502b2b',1,1,'authToken','[]',1,'2020-09-16 17:24:05','2020-09-16 17:24:05','2021-09-16 17:24:05'),('c24e59d63ec7f3a5919568d3ec6db74eaa9695573699c366ce161232ebacaee0eb9fbc7943fa7477',1,1,'authToken','[]',0,'2020-09-20 07:58:32','2020-09-20 07:58:32','2021-09-20 07:58:32'),('c2f40205550231a812b281697db8f4bc5fba294b2d52ab1f909884f05ca979f16567bdcc0eeaf138',1,1,'authToken','[]',0,'2020-09-20 08:13:06','2020-09-20 08:13:06','2021-09-20 08:13:06'),('c4751406f180335040bb48f728dccbe12a65b192c2b3000f9851669069bbe4898b099725cff58ad4',1,1,'authToken','[]',1,'2020-09-16 17:13:09','2020-09-16 17:13:09','2021-09-16 17:13:09'),('c493474bfd0e70468683df59acf6213c6fa1fb009799fab839c2d97503a6bc1adac43080bec10268',1,1,'authToken','[]',1,'2020-09-16 17:36:21','2020-09-16 17:36:21','2021-09-16 17:36:21'),('c4b332825b1298075308d02429317e14e8ac4e186061076582de8e1673e6ceafc662fc6d5b869186',1,1,'authToken','[]',1,'2020-09-16 17:22:05','2020-09-16 17:22:05','2021-09-16 17:22:05'),('c4d97db9a38ee4655a9c514a8519414966142ac8c9ab4b7d798f9c3dd12160d1900a29c24cbc3bbb',1,1,'authToken','[]',0,'2020-09-20 06:59:39','2020-09-20 06:59:39','2021-09-20 06:59:39'),('c543f95ed2027dd8eed0f60dfbf4cb91a9522938221173c9b2fa1e51c45b405a9c4061307d027479',1,1,'authToken','[]',1,'2020-09-16 17:36:50','2020-09-16 17:36:50','2021-09-16 17:36:50'),('c577c7cb04e09c3eb845d3fb41d7f07ac03161276d6339995c8c6340980675e8983b225f4ba92d63',1,1,'authToken','[]',0,'2020-09-20 07:04:33','2020-09-20 07:04:33','2021-09-20 07:04:33'),('c583db99b8931a368d2904a5491afb38b4906a6961137fa0e790462a0b098ba0013d4479ef594085',1,1,'authToken','[]',0,'2020-09-20 06:56:58','2020-09-20 06:56:58','2021-09-20 06:56:58'),('c5a55efbde195c47037f966cfdc09ffcdf336f58d27aa9fa4a7310b2de0c11069a94fa56fa89aa2a',1,1,'authToken','[]',1,'2020-09-16 13:39:03','2020-09-16 13:39:03','2021-09-16 13:39:03'),('c5b0ab7e37a5af341f463c2fde7a31db4ade24597961f913663a7aceb3df60c1e06ab85ea98ac04d',1,1,'authToken','[]',0,'2020-09-20 07:41:14','2020-09-20 07:41:14','2021-09-20 07:41:14'),('c64ad2a03599948957bcdb90652c7b753b2f3c56f42935b7392a32340946966b0c52b6423ee3885e',1,1,'authToken','[]',1,'2020-09-16 17:23:49','2020-09-16 17:23:49','2021-09-16 17:23:49'),('c6755356a535a95482e90a8434de5f6bf6a170c31c13efce73d6728ee724e48d8bdeae9c13bae7a7',1,1,'authToken','[]',1,'2020-09-16 17:28:55','2020-09-16 17:28:55','2021-09-16 17:28:55'),('c68a8af4ccba3410da664c0fa517bfd186fca48fc132e2426743b849e220b35e48e13d23c889c8e8',1,1,'authToken','[]',0,'2020-09-20 07:31:23','2020-09-20 07:31:23','2021-09-20 07:31:23'),('c701f36e9a2d2c72ce4ce5be65442b57248fa11b293b93a067c500e3250629c9a16c50b73b7f3ea1',1,1,'authToken','[]',1,'2020-09-16 15:03:12','2020-09-16 15:03:12','2021-09-16 15:03:12'),('c74cb5e3142f0ec6d759a403898248c01df2ceaa6ce7cbfaed2613819023986d55b643e820a98b59',1,1,'authToken','[]',1,'2020-09-16 17:26:53','2020-09-16 17:26:53','2021-09-16 17:26:53'),('c7a7d42d935dfbe72f1d2ae8ba26b313ded3ec3a2c1c7121b77d089495dd2246dca49b8edd4e1fa1',1,1,'authToken','[]',1,'2020-09-17 01:59:10','2020-09-17 01:59:10','2021-09-17 01:59:10'),('c7be4d965b8cbe45ed8d58f970f9fbefd13d123487326630576900dcaed51a0b0c3dbee41fe64b61',9,1,'authToken','[]',0,'2020-09-20 10:46:11','2020-09-20 10:46:11','2021-09-20 10:46:11'),('c7cfbaebe7242c96390b2cc9c3431e89d69be48f27b12590df7278de1574da78554b306ce392703c',1,1,'authToken','[]',1,'2020-09-16 17:12:12','2020-09-16 17:12:12','2021-09-16 17:12:12'),('c91628acc3448b5b4e3d38f6724f83b7e436e977bed5dfcee8b29f703608626edb17ae775ec1fa05',1,1,'authToken','[]',0,'2020-09-20 08:09:08','2020-09-20 08:09:08','2021-09-20 08:09:08'),('c9f53d0089029904707e813723270d209dbeffbfe12ef265df4097e9f3fec78435b36a43e2dd6bba',1,1,'authToken','[]',0,'2020-09-20 08:13:13','2020-09-20 08:13:13','2021-09-20 08:13:13'),('ca5e7889ee11779737e0c081ccf26c18231dec7256c67cbf42676264d80cb1f5e97227ab3845766a',1,1,'authToken','[]',1,'2020-09-17 01:43:12','2020-09-17 01:43:12','2021-09-17 01:43:12'),('ca7c4cbd3603562fd745d2f42d9bd960a8bc588ec645fb0cf15392c78589c5cff863c25cb19c38db',1,1,'authToken','[]',0,'2020-09-17 11:18:25','2020-09-17 11:18:25','2021-09-17 11:18:25'),('cb506ef003c2691474fb55124cbe801bed10548a9c6fc944a9b648822f3179ee49412f503a720cd5',1,1,'authToken','[]',1,'2020-09-17 01:55:18','2020-09-17 01:55:18','2021-09-17 01:55:18'),('ccc35074c6aed80fab9eaf9715881f2c65f77045aa00c1eef726f98ac3675a2e289bdae4da21dcc2',1,1,'authToken','[]',1,'2020-09-16 16:37:13','2020-09-16 16:37:13','2021-09-16 16:37:13'),('cd07893b8bd5425243b1bc41a03ebd1c07b019b285266948e711d341dabc905f022966970ba864e0',1,1,'authToken','[]',0,'2020-09-20 08:12:57','2020-09-20 08:12:57','2021-09-20 08:12:57'),('cdbeb44ed673ad9aad81712bd56fc51517227d91d8295588797605dc4bd1c72befaa5fc94415885d',10,1,'authToken','[]',0,'2020-09-20 11:19:41','2020-09-20 11:19:41','2021-09-20 11:19:41'),('cdc7b865b813cca33f58ebe799a51041ea9fad16846ade8af377b1498e36bc4e5c5269ecd32842a5',1,1,'authToken','[]',1,'2020-09-16 16:43:55','2020-09-16 16:43:55','2021-09-16 16:43:55'),('cde2f26ac64d5229a27d5e00a69ade107319a7904b00b2ac81599fb4d5f4ccc9eda3696b88eaf5b7',9,1,'authToken','[]',0,'2020-09-20 10:58:56','2020-09-20 10:58:56','2021-09-20 10:58:56'),('ce1c406207bf11de32bd1b0cb75441ce06cde5870d43c8f99908ecc0dc17d00e321844b656e959c5',1,1,'authToken','[]',0,'2020-09-20 07:58:07','2020-09-20 07:58:07','2021-09-20 07:58:07'),('cedd5cb0fd034b2df6016b0d077de280f73c70c46ad9bbfea2e12f1c6d7540f8fcb90a4060f6e53d',1,1,'authToken','[]',0,'2020-09-20 08:11:21','2020-09-20 08:11:21','2021-09-20 08:11:21'),('cf1a66663e064eaf764c3eef2383ea0aeb83a0207b0048ae9bcac6ef9616c09a730547f61244ec11',9,1,'authToken','[]',0,'2020-09-20 10:32:19','2020-09-20 10:32:19','2021-09-20 10:32:19'),('cf5d8bfc546d9b4b5993e85d801d89f689407a5b30dc60478f61c5b1606d31870ba46d1abc6dd044',1,1,'authToken','[]',0,'2020-09-20 07:20:40','2020-09-20 07:20:40','2021-09-20 07:20:40'),('cfba06fd96d0f7cd8252b3aaba276fd02d494c3d563219a29d3eff74f5027ca8d76b58ccb0d3907c',1,1,'authToken','[]',1,'2020-09-16 16:37:13','2020-09-16 16:37:13','2021-09-16 16:37:13'),('cfea319a570dd023ea984082367c9d5fc975f4d379ee30bbe232f42c5d990aa4f74152392d939ac3',1,1,'authToken','[]',0,'2020-09-18 12:24:26','2020-09-18 12:24:26','2021-09-18 12:24:26'),('d137311bb6eff8700dee42762b75312db943f407745992ae0ba1652e8534e31174211a63605de189',1,1,'authToken','[]',1,'2020-09-16 13:37:59','2020-09-16 13:37:59','2021-09-16 13:37:59'),('d14e4d0bb6935916d4dc8e52e68605709cae87480bf074f18025f8716cc37a0ba341c87571b254ac',1,1,'authToken','[]',0,'2020-09-18 01:01:55','2020-09-18 01:01:55','2021-09-18 01:01:55'),('d1b0dc18b731c8e340e2bc7cac803d512641341c882bfd8c95e2b9a2504c223a7b61368171d8a7ed',1,1,'authToken','[]',1,'2020-09-16 17:24:02','2020-09-16 17:24:02','2021-09-16 17:24:02'),('d2a6f5e5c658082b5ea9b127929f8159b1ee93195eb55dcad2b9847e1454c148f22ee4ebb624f862',1,1,'authToken','[]',0,'2020-09-18 01:01:52','2020-09-18 01:01:52','2021-09-18 01:01:52'),('d3292753ac6bcadd3caad4a5fcc8add38c6669ceca3a7fff963ff935a4e416fc9497f7f67c14bddb',1,1,'authToken','[]',0,'2020-09-20 08:10:09','2020-09-20 08:10:09','2021-09-20 08:10:09'),('d412b03dca044eed0cb66278a08e37b733179a472720d9f48722237465086a7fe1fc62697136bf88',1,1,'authToken','[]',1,'2020-09-17 01:43:08','2020-09-17 01:43:08','2021-09-17 01:43:08'),('d56b22394a4cd1198e8e1f2dd2d392837a39b7a9e651881faa11b4fea52625ce009b6aca4f31fae6',1,1,'authToken','[]',0,'2020-09-20 07:15:28','2020-09-20 07:15:28','2021-09-20 07:15:28'),('d5ba131dcdf23872bbad82211c509ca686d61ac4690653a024975348102189a6c72f94b46ca9ffb4',10,1,'authToken','[]',0,'2020-09-20 11:19:41','2020-09-20 11:19:41','2021-09-20 11:19:41'),('d5d4640071676163ec336d63bb5cdb8ed8122aa0b15223743e52336c38d46da215a9496e59595419',1,1,'authToken','[]',0,'2020-09-20 07:20:40','2020-09-20 07:20:40','2021-09-20 07:20:40'),('d642102854d2a3d01ced20b3a493d7892b754e14ef034f7c317f5695911d786eb2fc61db751b0fb0',9,1,'authToken','[]',0,'2020-09-20 10:57:58','2020-09-20 10:57:58','2021-09-20 10:57:58'),('d70c0a7b93b0e3a5f4386d39ef8d787935be1c3238613648ca5867d0deab2c09da672fe874aedfeb',1,1,'authToken','[]',0,'2020-09-20 07:31:23','2020-09-20 07:31:23','2021-09-20 07:31:23'),('d75a486bdd7b2f741cc940fff0d4cecfaab31a4c9d172e77e774446955a3b4aa877287433a0765d8',1,1,'authToken','[]',1,'2020-09-16 13:58:25','2020-09-16 13:58:25','2021-09-16 13:58:25'),('d7cc2328305a9b623507207d23c7712c37b4ca3bb7da7600455a84c5a987bd303ee505326280a4de',1,1,'authToken','[]',0,'2020-09-20 07:25:02','2020-09-20 07:25:02','2021-09-20 07:25:02'),('d7dec7e1dca7634cda7db97e4b187fdcf7fc60e59cdf9dba2b9017cef02f3acf8f70ab8eaf483186',1,1,'authToken','[]',0,'2020-09-18 12:07:10','2020-09-18 12:07:10','2021-09-18 12:07:10'),('d9b091b946ff954da5e9567557d19383149e2667fb9424d1dcb215291e3f2aeaa5206185a5c6c967',1,1,'authToken','[]',1,'2020-09-16 14:41:30','2020-09-16 14:41:30','2021-09-16 14:41:30'),('da0281ca8d5d82378a5e24122a9029e5f06abc120ae53e064a3ccbe123c4ca99a93a3bdf2eade5c1',1,1,'authToken','[]',1,'2020-09-17 01:58:55','2020-09-17 01:58:55','2021-09-17 01:58:55'),('da1acd243ba9f453073b8ca5613e54561f562195503632177bb89980a4a7e00bad9977e8fc05b15c',1,1,'authToken','[]',1,'2020-09-16 17:27:34','2020-09-16 17:27:34','2021-09-16 17:27:34'),('dae8cda0f5efd5454ca913b7adc6c0dbfeb65f805dc5290a29e87db0d5dd93cf1763005d1462b9df',1,1,'authToken','[]',0,'2020-09-20 07:12:28','2020-09-20 07:12:28','2021-09-20 07:12:28'),('daf868133389efc9c5925b0b26170b0374ac5455bf57560e7c614544a8be6a9394a592fbebc00456',1,1,'authToken','[]',0,'2020-09-20 07:12:58','2020-09-20 07:12:58','2021-09-20 07:12:58'),('db5bbe1ae3d9a858067b87d9d1cebb57ab30ccb6abea4e7573a552656e718420c74b23002d105a62',1,1,'authToken','[]',0,'2020-09-20 08:08:39','2020-09-20 08:08:39','2021-09-20 08:08:39'),('dba7d6e7b0e525d6447519bc51ba796971a0685c14f07dbe010f9c51a357d935cb40b840b67f5e42',1,1,'authToken','[]',0,'2020-09-18 01:01:59','2020-09-18 01:01:59','2021-09-18 01:01:59'),('dc2e2162f03d421baaec9c80a9b1194b7584019cd60bcd4c87220a7a4988ec97618f50d3a6968199',1,1,'authToken','[]',0,'2020-09-20 08:08:53','2020-09-20 08:08:53','2021-09-20 08:08:53'),('dc807240080063920cb501296501b9f2cfc822eee31404d79cbb50c3b97dd1ae4e89ff1a8a93b3d2',1,1,'authToken','[]',0,'2020-09-20 07:34:18','2020-09-20 07:34:18','2021-09-20 07:34:18'),('dc8c79f3f6c709dd8498d567bf295c32b75c44dd8f98c4f75e5b4b579c5133c11edcbab36181ad84',1,1,'authToken','[]',0,'2020-09-18 01:00:29','2020-09-18 01:00:29','2021-09-18 01:00:29'),('dd04cab3779e46eab0dcf9caae2d5ef936f8ff991c80af3f05e4dca7198805898aa5dd28a6ee94c3',1,1,'authToken','[]',0,'2020-09-18 01:01:54','2020-09-18 01:01:54','2021-09-18 01:01:54'),('de50ca894bcf9a96bde0a547521e086d9697615cedb1383cab3d194a2ed6851e1e04302c5ca7aee2',1,1,'authToken','[]',1,'2020-09-16 16:49:09','2020-09-16 16:49:09','2021-09-16 16:49:09'),('e054bf7e9416211f31c537c3dc36195d81c1b9607ecb0a1b9363b571046e3e386b5a828f2c28a6c2',9,1,'authToken','[]',0,'2020-09-20 10:45:02','2020-09-20 10:45:02','2021-09-20 10:45:02'),('e07bb54048faefbd03af6dd22a7c46f5e966298d996807650ebd1c87ba9d63c19cf5b513801620b7',1,1,'authToken','[]',0,'2020-09-20 07:17:08','2020-09-20 07:17:08','2021-09-20 07:17:08'),('e10bf9f9e81544e2f09d8a4f5040eed91d274a2c4c7519dbd867ec3fd2c94b949f890fa5aeabdd01',1,1,'authToken','[]',0,'2020-09-17 11:37:30','2020-09-17 11:37:30','2021-09-17 11:37:30'),('e11eff136b7f8d4386adbf1e1475f27bc6759cade8e73dc3e6594a83f50e88da091cbba9396e7426',1,1,'authToken','[]',0,'2020-09-20 08:11:11','2020-09-20 08:11:11','2021-09-20 08:11:11'),('e15d8717645bbec1690565547c21689c97d7c3b23b3790efffb24d2725387ab32cf106cbdc9ad2ac',1,1,'authToken','[]',1,'2020-09-16 17:28:16','2020-09-16 17:28:16','2021-09-16 17:28:16'),('e3eb63b6d5f7afed543805ff03a6e20f2274e748fac66c88891443409bf9b7e206297bb9fe61776a',1,1,'authToken','[]',1,'2020-09-16 16:35:02','2020-09-16 16:35:02','2021-09-16 16:35:02'),('e52246688f90e6ede73a5ebcc3e8e362a72d7fb6b9f4d9f3c723a3f40261b3d7cf133e430690a215',1,1,'authToken','[]',0,'2020-09-17 11:35:19','2020-09-17 11:35:19','2021-09-17 11:35:19'),('e66d81927d90af2e1fc23a50430cd8f514550759f970c64bcf59ef451e2c711673ac486bb014ca14',1,1,'authToken','[]',0,'2020-09-20 08:04:33','2020-09-20 08:04:33','2021-09-20 08:04:33'),('e76a2a2584ed2c710e676bc237acec74957e8349ac191d6a0464b4862219b649eef689166c67a7b2',1,1,'authToken','[]',1,'2020-09-16 13:34:00','2020-09-16 13:34:00','2021-09-16 13:34:00'),('e78fe3efe808402da8e9fc327899e748b030ae1f4d6c633564ebdc405d720e954cd2811c4a71fab0',1,1,'authToken','[]',0,'2020-09-18 12:24:44','2020-09-18 12:24:44','2021-09-18 12:24:44'),('e900053d8cd05cce64ee443bfe085beec19f40a65ae009ba89abc82d7531452643a78c7033bc4024',1,1,'authToken','[]',0,'2020-09-18 13:27:19','2020-09-18 13:27:19','2021-09-18 13:27:19'),('e93daa83b33cf54fa118aceb355675baabbdfad32f1ca75f45d5377bf983c5a7b198bb98c98ffa76',1,1,'authToken','[]',1,'2020-09-17 01:54:59','2020-09-17 01:54:59','2021-09-17 01:54:59'),('eafb7498805c2ccb4d2a6295c663ff1d2b5eae60d3c6a654280a06321ad10515a9ddf52446a741ba',1,1,'authToken','[]',0,'2020-09-18 12:20:27','2020-09-18 12:20:27','2021-09-18 12:20:27'),('eb4e32cd66c3feb03ee0ce6ee97d94c8a2aecb804a709a7cc537f536689bd5c5409547347a442eef',1,1,'authToken','[]',0,'2020-09-20 07:15:31','2020-09-20 07:15:31','2021-09-20 07:15:31'),('ebbb52f1e0c8b7540b473dff6c669228e23d2c3225cf88bebb931885db289e8c4386ce5e44de2781',1,1,'authToken','[]',0,'2020-09-20 08:09:08','2020-09-20 08:09:08','2021-09-20 08:09:08'),('ec0c8e946c73186d9b25a545d24f2a9f0c5e25ff4d9a56dc99afa93b010dac04b598852540bbea72',1,1,'authToken','[]',0,'2020-09-18 12:26:52','2020-09-18 12:26:52','2021-09-18 12:26:52'),('ed5aab726adc345c79533f6e5551d01542cd34397d886cd10428c7650e8e0ac238308b3ae406a2f9',1,1,'authToken','[]',1,'2020-09-16 13:48:33','2020-09-16 13:48:33','2021-09-16 13:48:33'),('ed7aa67e1d9f887df4e3c9e45ad44e2c1a89ce0cb0c55892d1e34a39a2842bdd58774ba8a52743c0',9,1,'authToken','[]',0,'2020-09-20 10:44:57','2020-09-20 10:44:57','2021-09-20 10:44:57'),('ee73f06112e38053e6ff01fd9a9f692a4e6428dc3ce9b09cc243cdb2a718f16f9e984e34503e10c6',1,1,'authToken','[]',1,'2020-09-16 15:58:09','2020-09-16 15:58:09','2021-09-16 15:58:09'),('eed80891a1da0d5b8568e57d7837addfe5a7950496ff2d22490e429027c175e10f20b65affd668fc',1,1,'authToken','[]',0,'2020-09-18 12:25:14','2020-09-18 12:25:14','2021-09-18 12:25:14'),('eef58b660c082682927f4e7b34f2476ed37dbbe5600d78fafaa6d56a0b76ef82815eae8c1adfed41',1,1,'authToken','[]',0,'2020-09-18 13:25:33','2020-09-18 13:25:33','2021-09-18 13:25:33'),('ef72023e6d5f84f434db92b8005e65703acbbb1b7d1a846135a8aaba71da44fd7c1acb5501df8abf',1,1,'authToken','[]',1,'2020-09-16 17:13:00','2020-09-16 17:13:00','2021-09-16 17:13:00'),('efafaac61a5d322ebfaef62285d4693eaad61b0c48a1a377070806e8f5c95d9bdf978ce732913b51',1,1,'authToken','[]',0,'2020-09-18 12:28:24','2020-09-18 12:28:24','2021-09-18 12:28:24'),('efb42eab8433b404b642cc73c24f6fdb74448f8ccd0370d7895574f200d227a3db6b06cb40427676',1,1,'authToken','[]',0,'2020-09-18 01:01:58','2020-09-18 01:01:58','2021-09-18 01:01:58'),('effc3e046bbbf34e979e12eb1d17a3ee67783bafbd8f2800391787138b5d5edb9efbf3f32a45ab01',1,1,'authToken','[]',1,'2020-09-17 01:43:46','2020-09-17 01:43:46','2021-09-17 01:43:46'),('f04faa278549765e0f16a48cfa65650abe8d8c7a7e71f3b224fdf0e35ca93e8c7a82ef6c5a2caf94',1,1,'authToken','[]',0,'2020-09-20 08:01:42','2020-09-20 08:01:42','2021-09-20 08:01:42'),('f1814a2b00dac039edcc1743c63300e49041d82cb18037392c8e8ac94dd84f9a8c9fcc0e3016e95b',1,1,'authToken','[]',1,'2020-09-16 12:10:38','2020-09-16 12:10:38','2021-09-16 12:10:38'),('f1c91faee7da728c905c4e80b682380aa867a0b2a01e95ad98119a40c2ae8d16012ce250a225cd83',1,1,'authToken','[]',0,'2020-09-20 07:04:07','2020-09-20 07:04:07','2021-09-20 07:04:07'),('f22fdf5a645882bd2bfec20ace15e5348984cd87b5285d070786de63521a90ab3e097a8c410d272f',1,1,'authToken','[]',1,'2020-09-16 13:47:18','2020-09-16 13:47:18','2021-09-16 13:47:18'),('f236ff76a27561cbae894a0b89a5d6fd07ecea9a03cfdd15e0a8fd11f04566fe597154b9986556a7',1,1,'authToken','[]',0,'2020-09-20 07:31:02','2020-09-20 07:31:02','2021-09-20 07:31:02'),('f28c540d7f190bf3ba5539b87e98ed21cac681a2dd850fbb5e0b206a751e93ea81c2be3587a04c66',1,1,'authToken','[]',1,'2020-09-16 17:15:54','2020-09-16 17:15:54','2021-09-16 17:15:54'),('f28f2c4bacfb981db608e9e3b9d35ef99cc6c3252b6175c6506b90a01198e717f9e8d3db45c3c7e0',1,1,'authToken','[]',0,'2020-09-20 06:59:48','2020-09-20 06:59:48','2021-09-20 06:59:48'),('f2fa314fa964115708d93251fcc2e675398137412b52abf484b3a2899a648f3323db29fb12e7c8b0',1,1,'authToken','[]',0,'2020-09-18 12:10:16','2020-09-18 12:10:16','2021-09-18 12:10:16'),('f31b6f5cfb01923d3970141efbb8c47cdc37b3ae3869f6bb6ee2fa30818fdb54c858d99343464fb8',1,1,'authToken','[]',1,'2020-09-16 17:26:56','2020-09-16 17:26:56','2021-09-16 17:26:56'),('f495177ebc0c7583b4205b5c16294ac2bd583ce0d959b15faa22c8890203f7111ef4721f9f6bc17b',1,1,'authToken','[]',0,'2020-09-20 07:31:26','2020-09-20 07:31:26','2021-09-20 07:31:26'),('f520c971e50f8f17e12d3775c28188a4edfd3c920e711e03fb9f02cb19c6183f8ed6a864485793db',1,1,'authToken','[]',0,'2020-09-17 11:19:01','2020-09-17 11:19:01','2021-09-17 11:19:01'),('f55ed46580165f131714733115df3e4885195a742c0d1cd64008654f6dc25ad741cd09c4686618a8',1,1,'authToken','[]',1,'2020-09-16 14:41:30','2020-09-16 14:41:30','2021-09-16 14:41:30'),('f5eb225078034f6ed8276bdd0d1f68a73059a667bc3c8501c33bc13135b89da94050f0f5503779f3',1,1,'authToken','[]',0,'2020-09-18 13:26:11','2020-09-18 13:26:11','2021-09-18 13:26:11'),('f6ed47b504e128dccf694bc6e09661a409aaffa15df175cf013983cf6d584a6db90aea44d6fa6ca0',1,1,'authToken','[]',0,'2020-09-20 07:01:58','2020-09-20 07:01:58','2021-09-20 07:01:58'),('f6f7aec1fd9f4f64f09171b6adb0f9a41e22f5d82041ecba8f4416d48ea40560261c1a9aae7ea2cc',1,1,'authToken','[]',1,'2020-09-17 09:39:15','2020-09-17 09:39:15','2021-09-17 09:39:15'),('f73e37f08515da0a8f4915f3db8cc2cee6fcd1cef78f7faec8ac3d1ed9b203fbfd60f5501180a4a1',1,1,'authToken','[]',0,'2020-09-20 07:58:28','2020-09-20 07:58:28','2021-09-20 07:58:28'),('f7879f65eb48174d1005be5ad249e0341ac7113f8a3740ebb3b76f5bc2735201c81ad79c50c3af68',1,1,'authToken','[]',1,'2020-09-16 13:48:34','2020-09-16 13:48:34','2021-09-16 13:48:34'),('f8339b4d578579111c5995c01c2640791f9c2faf6e5feec65a452591334e5108f87147bf181962f6',1,1,'authToken','[]',1,'2020-09-16 17:14:29','2020-09-16 17:14:29','2021-09-16 17:14:29'),('f8547be0abf8bf1a00becb9cf57fb70e0b30d66d4aa4f4f4f8d42c747d9abfd2ebd340ba9da1470b',1,1,'authToken','[]',1,'2020-09-16 17:22:25','2020-09-16 17:22:25','2021-09-16 17:22:25'),('f88327ee0a827a7a5e4d0236c895fd2001b8fd5f6c73ae4923c611bd3cd7c085023eafd2646ae2ac',1,1,'authToken','[]',0,'2020-09-18 01:00:29','2020-09-18 01:00:29','2021-09-18 01:00:29'),('f8fefb65e1698a24c4db68c90bfdfa55cd5b508814cba633ed33a1a76f5d2cb240ac21df8e789732',1,1,'authToken','[]',1,'2020-09-17 01:52:53','2020-09-17 01:52:53','2021-09-17 01:52:53'),('f90b734d5df15d5da24775862e7a728f66acace5bdae1eb612978783c7a0cdab5a7dae9d577225dd',1,1,'authToken','[]',0,'2020-09-18 12:20:37','2020-09-18 12:20:37','2021-09-18 12:20:37'),('f9779fcd3fe881fdec30f4579e9ae1f27e23ec68d0d03bcc37960c08911841fcff95b6b77cf9b0c7',1,1,'authToken','[]',1,'2020-09-16 13:47:18','2020-09-16 13:47:18','2021-09-16 13:47:18'),('f9985da87cccede5c06fc12432473e159de50f6c617c92ac56564c0b6515a6f217144a7c92f056db',1,1,'authToken','[]',1,'2020-09-16 17:23:48','2020-09-16 17:23:48','2021-09-16 17:23:48'),('f9f2e11ab5e73d33cf81e30fe7aa331358bca51c480824c5ad2c390d888c5b2608858a4deab42a34',1,1,'authToken','[]',0,'2020-09-20 06:46:33','2020-09-20 06:46:33','2021-09-20 06:46:33'),('fa2d79566ca93cf904d41ba85c0aa53f643673e7a09736c6fdedb426203a557c6396ab7935aa2bf2',1,1,'authToken','[]',1,'2020-09-17 01:49:10','2020-09-17 01:49:10','2021-09-17 01:49:10'),('fa6ff7f5282afe0ae4708c23f5e7c26794727e96c6e995d02d7a4830d2cab394b72dc481876b4a25',1,1,'authToken','[]',1,'2020-09-16 15:58:09','2020-09-16 15:58:09','2021-09-16 15:58:09'),('fa980e9e76e49971ffd3a12b65d15b0d8138b49b623764ed1eb08a0a599f46a5be95e0e79b947467',1,1,'authToken','[]',0,'2020-09-18 12:20:52','2020-09-18 12:20:52','2021-09-18 12:20:52'),('fb3c6b917c737d4613ac6881784b15e2c713135a54452f4fbeb48d6a9e5bf00fedb470e6d578e317',1,1,'authToken','[]',0,'2020-09-20 08:33:05','2020-09-20 08:33:05','2021-09-20 08:33:05'),('fba7bfa8b2d6b683260feca33656ea50de73383aaf94cd5e9c5388da1bca95da8498535ecec83df6',1,1,'authToken','[]',0,'2020-09-18 01:01:51','2020-09-18 01:01:51','2021-09-18 01:01:51'),('fbcbf29d6cb10cc2177c21b0de5edbdaa4107ff99f34cd881b94c25e7f628f5eb1ab6588e8217bdc',1,1,'authToken','[]',0,'2020-09-17 11:18:27','2020-09-17 11:18:27','2021-09-17 11:18:27'),('fd0cfe9278463496b37c40f49d196755091cecea5ab8f9bc25da120fd7ad818a92dbacd4d31d609b',1,1,'authToken','[]',0,'2020-09-18 14:36:21','2020-09-18 14:36:21','2021-09-18 14:36:21'),('feab5dd71390a8817deb187703ef0d038f9df5d8311f87a52043450d41a27b6788a695f86b2c719a',9,1,'authToken','[]',0,'2020-09-20 10:42:22','2020-09-20 10:42:22','2021-09-20 10:42:22'),('ffa8760005b3724623b50196027bad350da472db3f60f99fb98de00f3978d68c5bc0fe0165a705b3',1,1,'authToken','[]',0,'2020-09-20 08:11:11','2020-09-20 08:11:11','2021-09-20 08:11:11'),('ffd27e2de1b52ab48dd90c50a85288979975b5e55a20ff6b40f73cbff1fd181a9020e6aa476a0d2e',1,1,'authToken','[]',1,'2020-09-16 17:12:14','2020-09-16 17:12:14','2021-09-16 17:12:14');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES (1,NULL,'Online Betting Personal Access Client','Kyjjhb3nGGBOUccysf1ogvBKJqTql5LKSpaWRuhc',NULL,'http://localhost',1,0,0,'2020-09-16 12:09:57','2020-09-16 12:09:57'),(2,NULL,'Online Betting Password Grant Client','IKrqI4r70BfbW9awamgtGDM39BJkYbWOMO0fCOHf','users','http://localhost',0,1,0,'2020-09-16 12:09:57','2020-09-16 12:09:57');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,1,'2020-09-16 12:09:57','2020-09-16 12:09:57');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_types`
--

DROP TABLE IF EXISTS `status_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_types`
--

LOCK TABLES `status_types` WRITE;
/*!40000 ALTER TABLE `status_types` DISABLE KEYS */;
INSERT INTO `status_types` VALUES (1,'On-going',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(2,'Ended',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(3,'Upcoming',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10');
/*!40000 ALTER TABLE `status_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'bettor',
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_api_token_unique` (`api_token`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin@admin','admin@admin',NULL,'$2y$10$EUZhrpM2Na4PJ/uAQ8/j0uY9hMLRkBo.kbrw34374Fu1fULfpmwB2','5f6200a043c65','t6ugTqNHG9UK7CF3fMlAOVkIGHTTUmk2MYlNaccNxVjZv55zrzv1BiMtN4tU','2020-09-16 12:10:08','2020-09-16 12:10:08','admin',NULL),(2,'Paul','admin@admin.com',NULL,'$2y$10$EWtah8tUBGYU/nXgwczjTuQteRzFMnw37lYWfhMEaSoSlL/0UTqBG','5f67276c609bd',NULL,'2020-09-20 09:57:00','2020-09-20 09:57:00','bettor',NULL),(4,'Paul','admin2@admin.com',NULL,'$2y$10$KtuKCigQn4RzL8tW0Zqlae0kbM2cmqdCLlKgD.S/siOy2jzFcJVJW','5f672885c2f95',NULL,'2020-09-20 10:01:41','2020-09-20 10:01:41','bettor',NULL),(5,'Paul','admin3@admin.com',NULL,'$2y$10$TfajKiFScdYIGa.u0foCuePlaHuOVh6XN8ddRCV.icCblaajY/VA.','5f6728b679d44',NULL,'2020-09-20 10:02:30','2020-09-20 10:02:30','bettor',NULL),(6,'Paul','admin4@admin.com',NULL,'$2y$10$1h3E.zIRMI/v.lV.zdtbburk9t4qmKy0g7YrfptXsxQVBc/nLmfqy','5f6728dbc6867',NULL,'2020-09-20 10:03:07','2020-09-20 10:03:07','bettor',NULL),(7,'Paul','admin5@admin.com',NULL,'$2y$10$tps6hf3MWNc3WW8.nrH.huUQDVOTYqb4PWSdZ5rMiEheYYf4y0xvG','5f6728f40b567',NULL,'2020-09-20 10:03:32','2020-09-20 10:03:32','bettor','093914432929'),(8,'Paul','admin8@admin.com',NULL,'$2y$10$pddOeEBLlW8JQ3bxuHu.zuW6C2jBrVWkVGKpoH.opN4embWa7WBe6','5f672e0ac3fd7',NULL,'2020-09-20 10:25:14','2020-09-20 10:25:14','bettor','09394928382'),(9,'Paul','admin9@admin.co',NULL,'$2y$10$HkdfFxpw4Olo8bSGS/AlX.hj9rtf/azaq8fMrV195jFORmwAev4Vq','5f672fad1b99b',NULL,'2020-09-20 10:32:13','2020-09-20 10:32:13','bettor','09391442347'),(10,'admin10@admin','admin10@admin.com',NULL,'$2y$10$Iz2huxtaBEI0uGfSA/yPzeMfFGHHgRJqMm0GDnsweNfryVV4Xk2Ae','5f673ac849503',NULL,'2020-09-20 11:19:36','2020-09-20 11:19:36','bettor','093912029292');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `winners`
--

DROP TABLE IF EXISTS `winners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `winners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `game_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `bet_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `winners_game_id_foreign` (`game_id`),
  KEY `winners_user_id_foreign` (`user_id`),
  KEY `winners_bet_id_foreign` (`bet_id`),
  CONSTRAINT `winners_bet_id_foreign` FOREIGN KEY (`bet_id`) REFERENCES `bets` (`id`),
  CONSTRAINT `winners_game_id_foreign` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`),
  CONSTRAINT `winners_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `winners`
--

LOCK TABLES `winners` WRITE;
/*!40000 ALTER TABLE `winners` DISABLE KEYS */;
/*!40000 ALTER TABLE `winners` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-20 20:06:36
