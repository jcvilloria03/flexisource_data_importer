<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/user/login', 'API\UsersController@login');
Route::post('/user/signup', 'API\UsersController@signUp');

Route::get('/customers', 'CustomersController@index');
Route::get('/customers/{customerId}', 'CustomersController@get');
Route::put('/customers/{email}', 'CustomersController@update');
Route::post('/customers', 'CustomersController@populate');
