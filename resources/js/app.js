/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");
import Vue from 'vue'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import VueRouter from "vue-router";

Vue.use(Buefy);
Vue.use(VueRouter);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component(
    "example-component",
    require("./components/ExampleComponent.vue").default
);

Vue.component("menu-view", require("./components/App/Menu.vue").default);

Vue.component(
    "clients-index",
    require("./components/passport/Clients.vue").default
);
Vue.component(
    "personalaccesstokens-index",
    require("./components/passport/PersonalAccessTokens.vue").default
);

Vue.component(
    "header-component",
    require("./components/App/Header.vue").default
);

// Announcement
Vue.component(
    "announcement-add",
    require("./components/Announcements/AnnouncementAdd.vue").default
);


Vue.component(
    "announcement-edit",
    require("./components/Announcements/AnnouncementEdit.vue").default
);

Vue.component(
    "announcement-index",
    require("./components/Announcements/AnnouncementIndex.vue").default
);

const Foo = { template: "<div>foo</div>" };
const Bar = { template: "<div>bar</div>" };

const routes = [
    { path: "/foo", component: Foo },
    { path: "/bar", component: Bar }
];

const router = new VueRouter({
    mode: "history",
    routes: routes // short for `routes: routes`
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
    router,
    el: "#app"
});
