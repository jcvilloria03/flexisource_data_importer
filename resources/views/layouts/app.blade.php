<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Announcements</title>

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>

    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.materialdesignicons.com/5.3.45/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app">
        <b-navbar type="is-black">
            <template slot="brand">
                <b-navbar-item tag="router-link" :to="{ path: '/' }">
                    <h1>Announcements</h1>
                </b-navbar-item>
            </template>
            <template slot="start">
            <!--
                <b-navbar-item>

                </b-navbar-item>
                <b-navbar-item href="/events">
                    Events
                </b-navbar-item>
                <b-navbar-item href="/races">
                    Races
                </b-navbar-item>
                <b-navbar-item href="/bets">
                    Bets
                </b-navbar-item>
                <b-navbar-item href="/users">
                    Users
                </b-navbar-item>
                <b-navbar-item href="/horses">
                    Horses
                </b-navbar-item>
                <b-navbar-item href="/jockeys">
                    Jockeys
                </b-navbar-item>
                <b-navbar-item href="/loads">
                    Loads
                </b-navbar-item>
                <b-navbar-item href="/claims">
                    Claims
                </b-navbar-item>
                <b-navbar-item href="/settings">
                    Settings
                </b-navbar-item>
            -->
            </template>

            <template slot="end">
                @guest
                <b-navbar-item tag="div">
                    <div class="buttons">
                        <a href="{{ route('register') }}" class="button is-primary">
                            <strong>Register</strong>
                        </a>
                        <a href="{{ route('login') }}" class="button is-light">
                            Log in
                        </a>

                    </div>
                </b-navbar-item>

                @else
                <b-navbar-item>
                    {{ Auth::user()->name }}
                </b-navbar-item>
                <b-navbar-dropdown label="">

                    <b-navbar-item href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        Sign Out
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </b-navbar-item>
                </b-navbar-dropdown>
                @endif
            </template>
        </b-navbar>

        <main class="py-4 container is-fluid">
            <div class="columns">
                <div class="column">
                    @yield('content')
                </div>
            </div>
        </main>
    </div>
</body>

</html>